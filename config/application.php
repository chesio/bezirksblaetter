<?php

/**
 * Expose global env() function from oscarotero/env
 */
use function Env\env;

/**
 * Instruct Env\Env to read environment variables via $_ENV instead of getenv().
 * The latter does not work with DotEnv 5 by default, see: https://github.com/oscarotero/env/issues/8
 */
\Env\Env::$options |= \Env\Env::USE_ENV_ARRAY;

/** @var string Directory containing all of the site's files */
$root_dir = dirname(__DIR__);

/** @var string Document Root */
$webroot_dir = $root_dir . '/web';

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
$dotenv = \Dotenv\Dotenv::createImmutable($root_dir);
if (file_exists($root_dir . '/.env')) {
    $dotenv->load();
    $dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD', 'WP_HOME', 'WP_SITEURL']);
}

/**
 * Set up our global environment constant and load its config first
 * Default: production
 */
define('WP_ENVIRONMENT_TYPE', env('WP_ENVIRONMENT_TYPE') ?: 'production');

$env_config = __DIR__ . '/environments/' . WP_ENVIRONMENT_TYPE . '.php';

if (file_exists($env_config)) {
    require_once $env_config;
}

/**
 * URLs
 */
define('WP_HOME', env('WP_HOME'));
define('WP_SITEURL', env('WP_SITEURL'));

/**
 * Custom Content Directory
 */
define('CONTENT_DIR', '/app');
define('WP_CONTENT_DIR', $webroot_dir . CONTENT_DIR);
define('WP_CONTENT_URL', WP_HOME . CONTENT_DIR);

/**
 * DB settings
 */
define('DB_NAME', env('DB_NAME'));
define('DB_USER', env('DB_USER'));
define('DB_PASSWORD', env('DB_PASSWORD'));
define('DB_HOST', env('DB_HOST') ?: 'localhost');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');
$table_prefix = env('DB_PREFIX') ?: 'wp_';

/**
 * Authentication Unique Keys and Salts
 */
define('AUTH_KEY', env('AUTH_KEY'));
define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
define('NONCE_KEY', env('NONCE_KEY'));
define('AUTH_SALT', env('AUTH_SALT'));
define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
define('NONCE_SALT', env('NONCE_SALT'));

/**
 * Custom Settings
 */
define('AUTOMATIC_UPDATER_DISABLED', true);
define('DISABLE_WP_CRON', env('DISABLE_WP_CRON') ? true : false);
define('DISALLOW_FILE_EDIT', true);
define('WP_MEMORY_LIMIT', '128M');
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', env('WP_DEBUG_LOG') ?: false);
define('ROOT_DIR', $root_dir);

/**
 * Plugins etc.
 */
define('BC_SECURITY_GOOGLE_API_KEY', env('BC_SECURITY_GOOGLE_API_KEY'));
define('PLL_COOKIE', false); // See: https://polylang.pro/doc/is-polylang-compatible-with-the-eu-cookie-law/
define('WONOLOG_DISABLE', env('WONOLOG_DISABLE') ? true : false);

/**
 * Make sure HTTP_HOST and SERVER_NAME are defined when running WP-CLI, running WP Cron via PHP CLI etc.
 * https://make.wordpress.org/cli/handbook/common-issues/#php-notice-undefined-index-on-_server-superglobal
 */
if (php_sapi_name() === 'cli') {
    if (!isset($_SERVER['HTTP_HOST'])) {
        $_SERVER['HTTP_HOST'] = parse_url(WP_HOME, PHP_URL_HOST);
    }
    if (!isset($_SERVER['SERVER_NAME'])) {
        $_SERVER['SERVER_NAME'] = parse_url(WP_HOME, PHP_URL_HOST);
    }
}

/**
 * Bootstrap WordPress
 */
if (!defined('ABSPATH')) {
    define('ABSPATH', $webroot_dir . '/wp/');
}
