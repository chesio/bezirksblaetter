<?php
/**
 * Production
 */

define('SAVEQUERIES', false);
define('SCRIPT_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);
define('WP_POST_REVISIONS', 10);
define('DISALLOW_FILE_MODS', true);
define('WP_DISABLE_FATAL_ERROR_HANDLER', false);
define('STATIFY_IGNORE_REMOTE_ADDRESSES', explode(',', \Env\env('STATIFY_IGNORE_REMOTE_ADDRESSES')));
define('STATIC_URL_REPLACEMENTS', [
    'https://bezirksblaetter.cz/app/themes/bezirksblaetter/assets/dist/' => 'https://static.bezirksblaetter.cz/assets/',
    'https://bezirksblaetter.cz/app/uploads/' => 'https://static.bezirksblaetter.cz/uploads/',
    'https://bezirksblaetter.cz/wp/wp-includes/css/' => 'https://static.bezirksblaetter.cz/wp-includes-css/',
    'https://bezirksblaetter.cz/wp/wp-includes/js/' => 'https://static.bezirksblaetter.cz/wp-includes-js/',
]);

ini_set('display_errors', '0');
