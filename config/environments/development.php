<?php
/**
 * Development
 */

define('SAVEQUERIES', true);
define('SCRIPT_DEBUG', true);
define('WP_DEBUG_DISPLAY', true);
define('WP_POST_REVISIONS', false);
define('DISALLOW_FILE_MODS', false);
define('WP_DISABLE_FATAL_ERROR_HANDLER', true);

ini_set('display_errors', '1');
