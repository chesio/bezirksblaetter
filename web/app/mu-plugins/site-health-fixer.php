<?php
/**
 * Plugin Name: Site Health Fixer
 * Description: Removes obsolete Site Health checks.
 * Version: 20210316
 * Author: Česlav Przywara <ceslav@przywara.cz>
 * Author URI: https://wwww.chesio.com
 */

// See: https://github.com/roots/bedrock/issues/522
add_filter('site_status_tests', function (array $test_type): array {
    // Background updates are supposed to not work.
    unset($test_type['async']['background_updates']);

    // Debug log is enabled on purpose - it is not publicly available.
    unset($test_type['direct']['debug_enabled']);

    // Do not report inactive themes in development and non-available default theme in production.
    unset($test_type['direct']['theme_version']);

    return $test_type;
}, 10, 1);
