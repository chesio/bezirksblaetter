<?php
/**
 * Plugin Name: HTTP request tweaks
 * Description: Make external HTTP requests more reliable.
 * Version: 20230705
 * Author: Česlav Przywara <ceslav@przywara.cz>
 * Author URI: https://wwww.chesio.com
 */

/**
 * DNS resolution seems to be quite slow recently, so give DNS resolves some more time.
 */
add_filter('http_request_connect_timeout', function (): int { return 30; }, 20, 0);
add_filter('http_request_timeout', function (): int { return 30; }, 20, 0);

/**
 * What to say? HTTP 1.0 is ancient...
 */
add_filter('http_request_version', function (): string { return '1.1'; }, 10, 0);
