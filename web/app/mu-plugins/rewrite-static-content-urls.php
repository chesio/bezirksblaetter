<?php
/**
 * Plugin Name: Rewrite Static Content URLs
 * Description: Rewrite URLs of static assets like CSS and JS files or uploaded files in order to serve them from cookie-less (sub)domain.
 * Version: 20190227
 * Author: Česlav Przywara
 * Author URI: https://www.chesio.com/
 */

namespace Chesio\RewriteStaticContentUrls;

if (defined('STATIC_URL_REPLACEMENTS')) {
    call_user_func(
        function ($rewriter) {
            // Filter URLs of enqueued scripts
            add_filter('script_loader_src', [$rewriter, 'makeStatic'], 10, 1);

            // Filter URLs of enqueued stylesheets
            add_filter('style_loader_src', [$rewriter, 'makeStatic'], 10, 1);

            // Filter URLs of attachments (1)
            add_filter('wp_get_attachment_url', [$rewriter, 'makeStatic'], 10, 1);

            // Filter URLs of attachments (2)
            add_filter('wp_get_attachment_image_src', function ($image) use ($rewriter) {
                if (is_array($image)) {
                    $image[0] = $rewriter->makeStatic($image[0]);
                }
                return $image;
            }, 10, 1);

            // Filter URLs of attachments used in srcset attribute
            add_filter('wp_calculate_image_srcset', function (array $sources) use ($rewriter): array {
                foreach ($sources as $width => $source) {
                    $sources[$width]['url'] = $rewriter->makeStatic($source['url']);
                }
                return $sources;
            }, 10, 1);
        },
        new class (STATIC_URL_REPLACEMENTS) {
            /**
             * @var array
             */
            private $search;

            /**
             * @var array
             */
            private $replace;

            /**
             * @param array $replacements
             */
            public function __construct(array $replacements)
            {
                $this->search = array_keys($replacements);
                $this->replace = array_values($replacements);
            }

            /**
             * Apply static content URLs replacement rules on $url.
             *
             * @param string $url
             * @return string
             */
            public function makeStatic(string $url): string
            {
                return str_replace($this->search, $this->replace, $url);
            }
        }
    );
}
