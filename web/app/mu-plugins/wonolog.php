<?php
/**
 * Plugin Name: Wonolog Configurator
 * Plugin URI: https://github.com/inpsyde/Wonolog
 * Description: Configures Wonolog package.
 * Version: 20230709
 * Author: Inpsyde
 * Author URI: https://inpsyde.com/
 * License: MIT License
 */

// Set up additional handler that emails all errors to administrator.
add_action(
    'wonolog.setup',
    function (\Inpsyde\Wonolog\Configurator $config): void {
        $config->pushHandler(
            new \Monolog\Handler\NativeMailerHandler(
                get_option('admin_email'),
                sprintf('[%s] Error detected', get_option('blogname')),
                'wordpress@bezirksblaetter.cz',
                \Monolog\Logger::ERROR
            )
        );
    }
);

// Change location of log files written by default handler.
add_action(
    'wonolog.handler-setup',
    function (\Monolog\Handler\HandlerInterface $handler): void {
        if ($handler instanceof \Inpsyde\Wonolog\DefaultHandler\FileHandler) {
            $handler->withFolder(ROOT_DIR . DIRECTORY_SEPARATOR . 'wonolog');
        }
    }
);
