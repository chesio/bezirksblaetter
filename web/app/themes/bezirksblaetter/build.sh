#!/usr/bin/env bash
#
# Build theme assets.

echo 'Building theme assets ...'

# Grab theme directory
THEME_DIR=$(dirname $0)

# CD into assets directory
cd $THEME_DIR/assets || exit 1

# Update NPM modules
echo 'Updating NPM modules ...'
npm update

# Compile assets etc. using Gulp
echo 'Building assets (compiling CSS, minifying JS etc.) ...'
gulp build

# This seems to be necessary when script is run via Composer:
cd -

# Compile PO into MO files
echo 'Compiling language files ...'
cd $THEME_DIR/languages || exit 2
for po_file in $(ls *.po) ; do
	mo_file="$(basename -s .po $po_file).mo"
	msgfmt $po_file -o $mo_file
done

cd -

echo 'Done!'

exit 0
