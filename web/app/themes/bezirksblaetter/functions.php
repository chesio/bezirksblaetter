<?php
/**
 * Bezirksblätter bootstrap file
 *
 * @package Bezirksblätter
 */

// Option names
define('B13R_GALLERY_PAGE', 'b13r_gallery_page_id');
define('B13R_MEDIA_PARENT_PAGE', 'b13r_media_parent_page_id');
define('B13R_TAGS_PAGE', 'b13r_tags_page_id');
define('B13R_NO_OF_ITEMS_ON_HOMEPAGE', 'b13r_no_of_items_on_homepage');

// Project repo URL
define('B13R_REPO', 'https://gitlab.com/chesio/bezirksblaetter');


/**
 * Register simple autoloader for theme classes
 */
spl_autoload_register(function ($classname) {
    if (strpos($classname, 'CeP\\Bezirksblaetter') === 0) {
        // Get file name from class name
        $file = __DIR__ . '/src/classes/' . str_replace('\\', '/', substr($classname, 4)) . '.php';
        // Load the class definition, if file is readable.
        if (is_readable($file)) {
            require_once $file;
        }
    }
});

/**
 * Bezirksblätter includes
 */
$b13r_includes = [
    // Plugins
    'captcha',      // Comments captcha
    // Helpers
    'wordpress',    // Stuff that I miss in WordPress
    'helpers',      // Template helpers
    'template',     // Templating functions
    // Filters
    'taxonomies',   // Register custom taxonomies (for media)
    'attachments',  // Fixes for attachments handling in WordPress
    'setup',        // Theme setup
    'settings',     // Theme settings
    'admin',        // Backend adaptations
    'frontend',     // Frontend adaptations
    'integration',  // Integration with 3rd-party plugins
    'b13r',         // Customizations for bezirksblaetter.cz
];

array_walk($b13r_includes, function ($file) {
    if (!locate_template("src/{$file}.php", true, true)) {
        trigger_error("Error locating {$file} for inclusion.", E_USER_ERROR);
    }
});


/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/bezirksblaetter
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/bezirksblaetter/templates
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/bezirksblaetter
 *
 * We do this so that the Template Hierarchy will look in themes/bezirksblaetter/templates for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/bezirksblaetter
 *
 * This is not fully compatible with Live Preview without the use of a plugin to update the template option.
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/bezirksblaetter
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/bezirksblaetter
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/bezirksblaetter
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/bezirksblaetter/templates
 */

if (is_customize_preview()) {
    return;
}

add_filter('template', function ($stylesheet) {
    return dirname($stylesheet);
});

if (basename($stylesheet = get_option('template')) !== 'templates') {
    update_option('template', "{$stylesheet}/templates");
    wp_redirect($_SERVER['REQUEST_URI']);
    exit();
}
