/**
 * Gulp helps us:
 * 1) Compress JS files using Uglify
 * 2) Compile SASS into CSS
 * 3) Compile SVG files into single SVG sprite
 * 4) Grab dist files from NPM dependencies
 */

'use strict';

const del = require('del');
const exec = require('child_process').exec;
const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const eslint = require('gulp-eslint');
const filter = require('gulp-filter');
const gzip = require('gulp-gzip');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const svgmin = require('gulp-svgmin');
const svgstore = require('gulp-svgstore');
const uglify = require('gulp-uglify');


/**
 * Purge dist directory.
 *
 * https://www.npmjs.com/package/del
 */
const clean = function () {
    return del('dist');
};


/**
 * Compile CSS from SASS files for development use (expanded CSS with source maps)
 *
 * https://www.npmjs.com/package/gulp-sass
 */
const css_devel = function () {
    return gulp
        .src('src/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(
            sass.sync({
                // Include Node modules
                includePaths: [ 'node_modules' ],
                outputStyle: 'expanded'
            })
            .on('error', sass.logError)
        )
        .pipe(
            autoprefixer({ // list of supported browsers is in package.json
                cascade: false
            })
        )
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css'))
    ;
};

/**
 * Compile CSS from SASS files for production use (compressed CSS without source maps).
 *
 * https://www.npmjs.com/package/gulp-sass
 */
const css_prod = function () {
    return gulp
        .src('src/sass/*.scss')
        .pipe(
            sass.sync({
                // Include Node modules
                includePaths: [ 'node_modules' ],
                outputStyle: 'compressed'
            })
            .on('error', sass.logError)
        )
        .pipe(
            autoprefixer({ // list of supported browsers is in package.json
                cascade: false
            })
        )
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('dist/css'))
    ;
};

/**
 * Compile CSS from SASS files.
 */
const css = gulp.parallel(css_devel, css_prod);


/**
 * Copy font files to dist folder.
 */
 const fonts = function () {
    return gulp.src('src/fonts/**').pipe(gulp.dest('dist/fonts'));
};


/**
 * Combine svg icons into one file with <symbol> elements with IDs based on SVG filenames and remove any fill attributes
 * from path elements.
 *
 * Note: Maybe I missed something, but I found it impossible to change the fill color on xlink-ed elements, if they
 * have fill color applied by attribute like <path fill="#fff">.
 *
 * https://www.npmjs.com/package/gulp-svgstore
 * https://www.npmjs.com/package/gulp-svgmin
 */
const icons = function () {
    return gulp
        .src('src/icons/**/*.svg')
        .pipe(svgmin({
            plugins: [
                {
                    removeAttrs: { attrs: 'path:fill' },
                },
                {
                    removeViewBox: false
                }
            ]
        }))
        .pipe(svgstore()) // ~ produces single icons.svg file
        .pipe(gulp.dest('dist'))
    ;
};


/**
 * Optimize images using imagemin and additionaly gzip any SVG files.
 *
 * https://www.npmjs.com/package/gulp-imagemin
 */
const images = function () {
    return gulp
        .src('src/images/**')
        .pipe(imagemin({ verbose: true }))
        .pipe(gulp.dest('dist/images'))
        .pipe(filter(['**/*.svg']))
        .pipe(gzip({ append: true }))
        .pipe(gulp.dest('dist/images'))
    ;
};


/**
 * Check project custom JS with ESLint.
 *
 * https://www.npmjs.com/package/gulp-eslint
 * https://eslint.org/docs/user-guide/
 */
 const js_lint = function() {
    return gulp
        .src('src/js/*.js')
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
    ;
};

/**
 * Compress JS using Uglify.
 *
 * https://www.npmjs.com/package/gulp-uglify
 * https://github.com/gulpjs/gulp/blob/master/docs/recipes/minified-and-non-minified.md
 */
const js_uglify = function () {
    return gulp
        .src('src/js/*.js')
        .pipe(gulp.dest('dist/js'))
        .pipe(uglify())
        .pipe(rename({ extname: '.min.js' }))
        .pipe(gulp.dest('dist/js'))
    ;
};

const js = gulp.series(js_lint, js_uglify);


/**
 * Update Browserslist database.
 *
 * https://www.npmjs.com/package/browserslist#best-practices
 */
const update_browserslist = function (done) {
    exec(
        'npx browserslist@latest --update-db',
        {},
        function (error, stdout, stderr) {
            if (error) {
                throw error;
            }

            console.log(stdout);
            console.error(stderr);

            done();
        }
    );
};

/**
 * Update everything that needs to be updated once in a while.
 */
const update = gulp.series(update_browserslist);


/**
 * Copy Colcade script to /vendor and then minify it.
 */
const vendor_colcade = function () {
    return gulp
        .src('node_modules/+(colcade)/*.js')
        .pipe(gulp.dest('dist/vendor'))
        .pipe(uglify())
        .pipe(rename({ extname: '.min.js' }))
        .pipe(gulp.dest('dist/vendor'))
    ;
};

/**
 * Copy dist files from selected Node packages to dist/vendor folder.
 */
const vendor_verbatim = function () {
    return gulp
        .src([
            'node_modules/+(photoswipe)/dist/**/*.*',
        ])
        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace('/dist', '');
        }))
        .pipe(gulp.dest('dist/vendor'))
    ;
};

const vendor = gulp.parallel(vendor_colcade, vendor_verbatim);


/**
 * Perform a clean build for deployment.
 */
const build = gulp.series(
    gulp.parallel(clean, update),
    gulp.parallel(fonts, icons, images, js, css, vendor)
);


/**
 * Watch for changes.
 */
const watch = function () {
    gulp.watch('node_modules/(colcade|photoswipe)/**', vendor);
    gulp.watch('src/fonts/**', fonts);
    gulp.watch('src/icons/**', icons);
    gulp.watch('src/images/**', images);
    gulp.watch('src/js/**', js);
    gulp.watch('src/sass/**', css);
};


// Export main tasks

exports.clean = clean;
exports.css = css;
exports.fonts = fonts;
exports.icons = icons;
exports.images = images;
exports.js = js;
exports.vendor = vendor;

exports.build = build;
exports.watch = watch;

// Default task: perform a clean build and watch for changes.
exports.default = gulp.series(build, watch);
