/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * CeP: Refactored to use vanilla JavaScript only.
 * ======================================================================== */

(function() {

    var html = document.querySelector('html');

    function computeHeight(classname) {
        // eg. vh-100 or vmin-75
        var parts = classname.split('-');
        var size = parseInt(parts[1]) / 100;

        switch (parts[0]) {
            case 'vh':
                return window.innerHeight * size;
            case 'vmax':
                return Math.max(window.innerWidth, window.innerHeight) * size;
            case 'vmin':
                return Math.min(window.innerWidth, window.innerHeight) * size;
            case 'vw':
                return window.innerWidth * size;
            default:
                return 0;
        }
    }

    /**
     * @link https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API
     * @param {String} type Either localStorage or sessionStorage.
     * @param {String} key
     * @param {String} value
     * @return {Boolean}
     */
    function store(type, key, value) {
        try {
            window[type].setItem(key, value);
            return true;
        }
        catch(e) {
            return false;
        }
    }

    function toggleZoomState() {
        // Change page state.
        var zoomed_out = html.classList.toggle('zoomed-out');

        // Store current state.
        store('sessionStorage', 'zoom', zoomed_out ? 'out' : 'in');
    }

    /**
     * Toggle fullscreen mode for given element
     */
    function toggleFullscreen(element) {
        // https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API
        var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;

        if (requestMethod) {
            requestMethod.call(element);
        }
    }

    function initFullscreenSwitch(element, toggler) {
        if (element && toggler) {
            toggler.addEventListener('click', function() {
                toggleFullscreen(element);
            });
        }
    }

    /**
     * Fix image header height, because vh units are problematic on mobile:
     * @link https://github.com/bokand/URLBarSizing
     */
    function initImageHeaderHeightFixer(imageheader) {

        if (!imageheader) {
            return;
        }

        // Height is determined from classname (like vh-100 or vmin-75 etc.)
        var classname = null;
        var pattern = /^v(h|min|max|w)-\d+$/i;

        for (var i = 0; i < imageheader.classList.length; ++i) {
            var cn = imageheader.classList.item(i);
            if (pattern.test(cn)) {
                classname = cn;
                break;
            }
        }

        if (!classname) {
            return;
        }

        var img = imageheader.querySelector('img');

        if (!img) {
            return;
        }

        var height = 0; // last recorded height
        var timeout = null;

        function fixImageHeight() {
            var new_height = computeHeight(classname);
            if (new_height === height) {
                // Wait for orientation change event to finish...
                if (timeout) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(fixImageHeight, 100);
            }
            else {
                // Ok, update image height.
                height = new_height;
                img.style.height = new_height + "px";
            }
        }

        fixImageHeight();
        window.addEventListener('orientationchange', fixImageHeight);
    }

    function initImageNavi(linkPrev, linkNext) {
        // Grab URLs of prev/next image
        var prevLocation = linkPrev ? linkPrev.getAttribute('href') : null;
        var nextLocation = linkNext ? linkNext.getAttribute('href') : null;

        // To prevent firing when keys combination (like Alt+Left) is pressed
        var keysPressed = [];

        document.addEventListener('keydown', function(event) {
            keysPressed.push(event.which);

            if (keysPressed.length > 1) {
                // Do not fire, there's more than one key pressed.
                return;
            }

            // Do not fire, if inside form element.
            var target = event.target || event.srcElement;
            switch (target.tagName.toLowerCase()) {
                case 'input':
                case 'select':
                case 'textarea':
                    return;
            }

            switch (event.which) {
                case 37: // left
                    if (prevLocation) { location.assign(prevLocation); }
                    break;
                case 39: // right
                    if (nextLocation) { location.assign(nextLocation); }
                    break;
            }
        });

        document.addEventListener('keyup', function(event) {
            // Update the array, remove current key.
            keysPressed = keysPressed.filter(function(key) {
                return key !== event.which;
            });
        });
    }

    function initImageZoom(element) {
        if (element) {
            element.addEventListener('click', toggleZoomState);
        }
    }

    function initModSwitcher(element) {
        if (element) {
            element.addEventListener('click', function() {
                // Change page state.
                var lights_off = html.classList.toggle('lights-off');

                // Remember current state.
                store('localStorage', 'lights', lights_off ? 'off' : 'on');
            });
        }
    }

    function enhanceExternalLinks(container) {
        // The endsWith method is not supported in IE:
        // https://www.w3schools.com/jsref/jsref_endswith.asp
        try { '.'.endsWith('.'); } catch (e) { return; }

        if (container) {
            var hostname = window.location.hostname;
            var links = container.querySelectorAll('[href^="http://"], [href^="https://"]');
            for (var i = 0; i < links.length; ++i) {
                var link = links[i];
                var parsedUrl = new URL(link.getAttribute('href')); // https://developer.mozilla.org/en-US/docs/Web/API/URL
                if (parsedUrl.hostname === hostname || parsedUrl.hostname.endsWith('.' + hostname)) {
                    // Local link, ignore.
                    continue;
                }
                if (link.children.length > 1) {
                    // Not a plain text link, ignore.
                    continue;
                }
                // Replace text content with (wrapped) content including SVG icon.
                link.classList.add('external-link');
                link.innerHTML = window.b13r.content_with_svg_icon(link.textContent, '', 'external-link');
            }
        }
    }

    function enhancePhotoSwipeLinks(container) {
        if (container) {
            var links = container.querySelectorAll('a[href]');
            for (var i = 0; i < links.length; ++i) {
                var link = links[i];

                if (link.classList.contains('pswp-link')) {
                    // Ignore proper PhotoSwipe links.
                    continue;
                }

                var linkEl = document.querySelector('a.pswp-link[href="' + link.getAttribute('href') + '"]');
                if (linkEl) {
                    // Set class to distinguish the link.
                    link.classList.add('photoswiped-link');
                    // Save element's target.
                    link.pswp = linkEl;
                    // Replace text content with (wrapped) content including SVG icon.
                    link.innerHTML = window.b13r.content_with_svg_icon(link.textContent, '', 'image');
                }
            }
        }
    }

    function enhanceNaviSwitcher(navTrigger, primaryNav) {
        // Keep track of location.hash at the moment when navi is opened.
        var initialHash;

        navTrigger.addEventListener('change', function() {
            if (navTrigger.checked) {
                // Store hash at the time navigation opens.
                initialHash = location.hash;
                location.hash = 'menu-open';
            } else if (location.hash === '#menu-open') {
                // Just revert hash change made above.
                initialHash = null;
                history.back();
            }
            // Inform primary nav about the display change.
            primaryNav.dispatchEvent(new CustomEvent('primary-nav-toggle', {detail: {isOpen: navTrigger.checked}}));
        });

        // Pressing back button closes the navigation
        window.addEventListener('hashchange', function() {
            if (location.hash === initialHash) {
                navTrigger.checked = false;
                // Inform primary nav about the display change.
                primaryNav.dispatchEvent(new CustomEvent('primary-nav-toggle', {detail: {isOpen: navTrigger.checked}}));
            }
        });

        // Pressing Esc button closes the navigation
        document.addEventListener('keyup', function(event) {
            if (event.keyCode === 27) {
                navTrigger.checked = false;
                // Checkbox change event has to be triggered manually:
                var htmlEvent = document.createEvent('HTMLEvents');
                htmlEvent.initEvent('change', true, false);
                navTrigger.dispatchEvent(htmlEvent);
            }
        });
    }


    /**
     * @link https://developers.google.com/web/fundamentals/accessibility/focus/using-tabindex
     */
    function fixPrimaryNavFocus(primaryNav, navTrigger) {
        // Manage focus of all link elements within primary navigation.
        var links = primaryNav.querySelectorAll('a');

        var manageFocus = function (isPrimaryNavOpen) {
            var tabindex = isPrimaryNavOpen ? 0 : -1;
            for (var i = 0; i < links.length; ++i) {
                links[i].setAttribute('tabindex', tabindex);
            }
        };

        // Run on init...
        manageFocus(navTrigger.checked);
        // ...and whenever primary nav toggle status changes.
        primaryNav.addEventListener('primary-nav-toggle', function (event) { manageFocus(event.detail.isOpen); });
    }


    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Bezirksblaetter = {
        // All pages
        common: {
            init: function() {
                // JavaScript to be fired on all pages
                initModSwitcher(document.querySelector('button#turn-lights-on'));
                initModSwitcher(document.querySelector('button#turn-lights-off'));
                enhanceExternalLinks(document.querySelector('main'));
                enhancePhotoSwipeLinks(document.querySelector('main'));
                enhanceNaviSwitcher(document.querySelector('input#nav-trigger'), document.querySelector('#primary-nav'));
                fixPrimaryNavFocus(document.querySelector('#primary-nav'), document.querySelector('input#nav-trigger'));
            },
            finalize: function() {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Attachment page (both image and video)
        attachment: {
            init: function() {
                // JavaScript to be fired on the attachment page
                initImageNavi(document.querySelector('a[rel="prev"]'), document.querySelector('a[rel="next"]'));
            },
            finalize: function() {
                // JavaScript to be fired on the attachment page, after the init JS
            }
        },
        // Image attachment page
        attachment_image: {
            init: function() {
                // JavaScript to be fired on the image attachment page
                initImageZoom(document.querySelector('div#image-header img'));
                initImageZoom(document.querySelector('button#zoom-in'));
                initImageZoom(document.querySelector('button#zoom-out'));
            },
            finalize: function() {
                // JavaScript to be fired on the image attachment page, after the init JS
            }
        },
        // Image attachment page
        attachment_video: {
            init: function() {
                // JavaScript to be fired on the video attachment page
                initFullscreenSwitch(document.querySelector('video#fullscreen-element'), document.querySelector('button#fullscreen-toggler'));
            },
            finalize: function() {
                // JavaScript to be fired on the video attachment page, after the init JS
            }
        },
        // Page with image header
        image_header_on: {
            init: function() {
                initImageHeaderHeightFixer(document.querySelector('div#image-header'));
            },
            finalize: function() {
                // JavaScript to be fired on the image header page, after the init JS
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Bezirksblaetter;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            var body_classes = document.body.className.replace(/-/g, '_').split(/\s+/);
            for (var i = 0; i < body_classes.length; ++i) {
                UTIL.fire(body_classes[i]);
                //UTIL.fire(body_classes[i], 'finalize');
            }

            // Fire common finalize JS
            //UTIL.fire('common', 'finalize');
        }
    };


    /**
     * Vanilla JavaScript alternative to $(document).ready() from jQuery:
     * http://beeker.io/jquery-document-ready-equivalent-vanilla-javascript
     *
     * @param {callback} callback
     * @returns {undefined}
     */
    var domReady = function(callback) {
        document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
    };

    // Load Events
    domReady(UTIL.loadEvents);

})();
