/* global Colcade */

/**
 * Enhance media grid with Colcade
 */

(function(window, document, Colcade) {

    // Do not Lazy Daisy invoke Yall.js automatically.
    window.lazyDaisy = function () {};

    function initMediaGrid() {
        // Note: this selects only the first media grid on a page
        var grid = document.querySelector('.media-grid');
        if (grid) {
            new Colcade(grid, {
                columns: '.grid-column',
                items: '.grid-item'
            });
        }

        // Lazy load the bitches!
        if (typeof window.yall === 'function') { window.yall(); }
    }

    /**
     * Vanilla JavaScript alternative to $(document).ready() from jQuery:
     * http://beeker.io/jquery-document-ready-equivalent-vanilla-javascript
     *
     * @param {callback} callback
     * @returns {undefined}
     */
    var domReady = function(callback) {
        document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
    };

    // Load Events
    domReady(initMediaGrid);

})(window, document, Colcade);
