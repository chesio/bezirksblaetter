/* global PhotoSwipe, PhotoSwipeUI_Default */

/**
 * PhotoSwipe initialization script
 *
 * Expected gallery markup:
 * <div data-gallery="chesio-on-the-beach">
 *
 *   <div class="pswp-item" data-item-index="0">
 *     <figure>
 *       <a href="big-and-naked.jpg" class="pswp-link" title="Naked Chesio" data-pid="big-and-naked" data-width="3648" data-height="2736" data-title="" data-caption="" data-large-url="big-and-naked-2048x1536.jpg" data-large-width="2048" data-large-height="1536" data-medium-url="big-and-naked-1024x768.jpg" data-medium-width="1024" data-medium-height="768">
 *         <img src="small-and-naked.jpg" />
 *       </a>
 *     </figure>
 *   </div>
 *
 *   [...]
 *
 * </div>
 *
 * Note: any items in the gallery without "pswp-item" are ignored.
 */
(function(window, document, Math, PhotoSwipe, PhotoSwipeUI) {


    /**
     * jQuery.extend() replacement
     * @param {object} o1
     * @param {object} o2
     * @returns {undefined}
     */
    var extend = function(o1, o2) {
        for (var prop in o2) {
            if (Object.prototype.hasOwnProperty.call(o2, prop)) {
                o1[prop] = o2[prop];
            }
        }
    };


    var isDimRatioEqual = function(items, epsilon) {
        for (var i = 0; i < items.length; ++i) {

            var item = items[i];

            if (!item.thumb) {
                // No thumbnail image
                return false;
            }

            if (Math.abs((item.w/item.thumb.scrollWidth) - (item.h/item.thumb.scrollHeight)) > epsilon) {
                // Not equal
                return false;
            }
        }

        return true;
    };


    var parseHash = function() {
        var hash = window.location.hash.substring(1);
        var params = {};

        if (hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');

        for (var i = 0; i < vars.length; i++) {
            if (!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if (pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        return params;
    };


    /**
     * Parse gallery items into array.
     * @param {Object} gallery
     * @returns {Array}
     */
    var parseGalleryItems = function(gallery) {

        var items = [], _items = gallery.getElementsByClassName('pswp-item'), n = _items.length;

        for (var i = 0; i < n; ++i) {

            var _item = _items[i];

            var figureEl = (_item.tagName.toLowerCase() === 'figure') ? _item : _item.firstElementChild; // <figure> element
            var linkEl = figureEl.getElementsByClassName('pswp-link')[0]; // <a> element

            // Build title
            var title = '<strong>' + (linkEl.getAttribute('data-title') ? linkEl.getAttribute('data-title') : linkEl.getAttribute('title')) + '</strong>';

            // Attach arrow icon
            title = window.b13r.content_with_svg_icon(title, '', 'long-arrow-right');

            // If image has a detail page link, link to it via title.
            if (figureEl.getElementsByClassName('pswp-detail').length > 0) {
                var detailLinkEl = figureEl.getElementsByClassName('pswp-detail')[0];
                title = '<a href="' + detailLinkEl.getAttribute('href') + '">' + title + '</a>';
            }
            else if (linkEl.getAttribute('data-detail')) {
                title = '<a href="' + linkEl.getAttribute('data-detail') + '">' + title + '</a>';
            }

            // Display caption in second line below title: prefer content of figcaption
            if (figureEl.getElementsByTagName('figcaption').length > 0) {
                var figcaptionEl = figureEl.getElementsByTagName('figcaption')[0];
                title += '<br><span>' + figcaptionEl.innerHTML + '</span>';
            }
            else if (linkEl.getAttribute('data-caption')) {
                title += '<br><span>' + linkEl.getAttribute('data-caption') + '</span>';
            }

            var item = {
                fullImage: {
                    src: linkEl.getAttribute('href')
                },
                pid: linkEl.getAttribute('data-pid'),
                title: title
            };

            // Get image dimensions
            var meta = figureEl.getElementsByTagName('meta');
            for (var j = 0; j < meta.length; ++j) {
                var itemprop = meta[j].getAttribute('itemprop');
                var content = meta[j].getAttribute('content');

                if (itemprop === 'width') {
                    item.fullImage.w = parseInt(content);
                }
                else if (itemprop === 'height') {
                    item.fullImage.h = parseInt(content);
                }
            }

            // If no width/height has been passed by meta, fall back to link data attributes.
            if (!item.w) {
                item.fullImage.w = parseInt(linkEl.getAttribute('data-width'));
            }
            if (!item.h) {
                item.fullImage.h = parseInt(linkEl.getAttribute('data-height'));
            }

            // Get source of thumbnail image
            var images = linkEl.getElementsByTagName('img');
            if (images.length === 1) {
                item.thumb = images[0]; // for isDimRatioEqual() and getThumbBoundsFn()
                item.msrc = images[0].getAttribute('src');
            }

            // Responsive images: medium (up to 1024px)
            var mediumUrl = linkEl.getAttribute('data-medium-url');
            var mediumWidth = parseInt(linkEl.getAttribute('data-medium-width'));
            var mediumHeight = parseInt(linkEl.getAttribute('data-medium-height'));

            if (mediumUrl && mediumWidth && mediumHeight) {
                item.mediumImage = {
                    src: mediumUrl,
                    w: mediumWidth,
                    h: mediumHeight
                };
            }

            // Responsive images: medium (up to 2048px)
            var largeUrl = linkEl.getAttribute('data-large-url');
            var largeWidth = parseInt(linkEl.getAttribute('data-large-width'));
            var largeHeight = parseInt(linkEl.getAttribute('data-large-height'));

            if (largeUrl && largeWidth && largeHeight) {
                item.largeImage = {
                    src: largeUrl,
                    w: largeWidth,
                    h: largeHeight
                };
            }

            // Items order within photo gallery has to be provided explicitly.
            var index = parseInt(_item.getAttribute('data-item-index'));
            items[index] = item;
        }

        return items;
    };


    /**
     * Initialize PhotoSwipe viewer
     * @param {object} template Root DOM Element with PhotoSwipe template
     * @returns {photoswipeL#6.Viewer}
     */
    var Viewer = function(template) {
        this.template = template;
        this.epsilon = template.getAttribute('data-epsilon') ? parseFloat(template.getAttribute('data-epsilon')) : 0.1;
        this.options = template.getAttribute('data-options') ? template.getAttribute('data-options') : {};
        // Up to given breakpoint, given responsive size is served
        this.mediumBreakpoint = 1024;
        this.largeBreakpoint = 2048;
    };

    extend(Viewer.prototype, {

        init: function() {
            // Add click handlers to all galleries
            var galleries = document.querySelectorAll('[data-gallery]');
            for (var i = 0; i < galleries.length; ++i) {
                galleries[i].addEventListener('click', this.handleThumbnailClick.bind(this));
            }

            // Add click handler to capture clicks on in-text links to gallery items.
            document.querySelector('main').addEventListener('click', this.handleAnchorClick.bind(this));

            var hashData = parseHash();

            if (hashData.gid && hashData.pid) {
                // Does pid refer to existing item?
                var linkEl = document.querySelector('.pswp-link[data-pid="' + hashData.pid + '"]');
                if (!linkEl) {
                    return;
                }

                // Does gid refer to existing gallery?
                var galleryEl = document.querySelector('[data-gallery="' + hashData.gid + '"]');
                if (!galleryEl) {
                    return;
                }

                this.openGalleryFromLink(linkEl, galleryEl);
            }
        },


        handleAnchorClick: function(event) {
            // Grab event.
            event = event || window.event;

            // Grab clicked item.
            var target = event.target || event.srcElement;

            // Grab Photoswipe-d link (if any)
            var anchor = target.closest('a.photoswiped-link');

            // If not a proper PhotoSwipe-d link, bail.
            if (!(anchor && ('pswp' in anchor))) {
                return;
            }

            // Attempt to open corresponding PhotoSwipe item.
            if (this.openGalleryFromLink(anchor.pswp)) {
                event.preventDefault ? event.preventDefault() : event.returnValue = false; // !
            }
        },


        handleThumbnailClick: function(event) {
            // Grab event.
            event = event || window.event;

            // Grab clicked item.
            var target = event.target || event.srcElement;

            // Grab PhotoSwipe link and attempt to open it.
            var linkEl = target.closest('a.pswp-link');
            if (linkEl && this.openGalleryFromLink(linkEl)) {
                event.preventDefault ? event.preventDefault() : event.returnValue = false;
            }
        },


        openGalleryFromLink: function(linkEl, galleryEl) {
            // Grab clicked grid item.
            var gridItemEl = linkEl.closest('.pswp-item');
            if (!gridItemEl) {
                return false;
            }

            // If gallery element is provided...
            if (galleryEl) {
                // ...check that provided PhotoSwipe link belongs to the gallery...
                if (gridItemEl.closest('[data-gallery]') !== galleryEl) {
                    return false;
                }
            } else {
                // ...otherwise just get parent gallery.
                galleryEl = gridItemEl.closest('[data-gallery]');
                if (!galleryEl) {
                    return false;
                }
            }

            // Open the gallery.
            this.openPhotoSwipe(galleryEl, parseInt(gridItemEl.getAttribute('data-item-index')));

            return true;
        },


        openPhotoSwipe: function(galleryEl, index) {

            var items = parseGalleryItems(galleryEl);

            if (!((0 <= index) && (index < items.length))) {
                // Index out of bounds, cannot proceed.
                return;
            }

            extend(this.options, {
                index: index,
                galleryPIDs: true,
                galleryUID: galleryEl.getAttribute('data-gallery'),
                loop: false
            });

            var getThumbBoundsFn = function(index) {
                var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
                var rect = items[index].thumb.getBoundingClientRect();

                return { x:rect.left, y:rect.top + pageYScroll, w:rect.width };
            };

            if (isDimRatioEqual(items, this.epsilon)) {
                this.options.showHideOpacity = false;
                this.options.getThumbBoundsFn = getThumbBoundsFn;
                this.template.classList.remove('pswp--no-placeholder');
            }
            else {
                this.options.showHideOpacity = true;
                this.options.getThumbBoundsFn = false;
                this.template.classList.add('pswp--no-placeholder');
            }

            var gallery = new PhotoSwipe(this.template, PhotoSwipeUI, items, this.options);

            // Hide prev/next button when at the first/last slide.
            // Note: navigating via keyboard arrows is not affected by the change below
            // https://github.com/dimsemenov/PhotoSwipe/issues/1028#issuecomment-206124833
            var prevButton = this.template.querySelector('.pswp__button--arrow--left');
            var nextButton = this.template.querySelector('.pswp__button--arrow--right');
            gallery.listen('beforeChange', function() {
                prevButton.style.display = (this.options.loop || this.getCurrentIndex() > 0) ? '' : 'none';
                nextButton.style.display = (this.options.loop || this.getCurrentIndex() < (this.items.length - 1)) ? '' : 'none';
            });

            // http://photoswipe.com/documentation/responsive-images.html
            var that = this;
            var imageSize = 'full'; // default
            var firstResize = true;

            gallery.listen('beforeResize', function() {
                //
                var realViewportWidth = gallery.viewportSize.x * window.devicePixelRatio;
                var invalidate = false;

                if (realViewportWidth > that.largeBreakpoint) {
                    invalidate = (imageSize !== 'full') && !firstResize;
                    imageSize = 'full';
                }
                else if (realViewportWidth > that.mediumBreakpoint) {
                    invalidate = (imageSize !== 'large') && !firstResize;
                    imageSize = 'large';
                }
                else {
                    invalidate = (imageSize !== 'medium') && !firstResize;
                    imageSize = 'medium';
                }

                if (invalidate) {
                    gallery.invalidateCurrItems();
                }

                firstResize = false;
            });

            gallery.listen('gettingData', function(index, item) {
                if ((imageSize === 'medium') && item.mediumImage) {
                    item.src = item.mediumImage.src;
                    item.w = item.mediumImage.w;
                    item.h = item.mediumImage.h;
                }
                else if ((imageSize === 'large') && item.largeImage) {
                    item.src = item.largeImage.src;
                    item.w = item.largeImage.w;
                    item.h = item.largeImage.h;
                }
                else {
                    item.src = item.fullImage.src;
                    item.w = item.fullImage.w;
                    item.h = item.fullImage.h;
                }
            });

            gallery.init();
        }
    });

    /**
     * Vanilla JavaScript alternative to $(document).ready() from jQuery:
     * http://beeker.io/jquery-document-ready-equivalent-vanilla-javascript
     *
     * @param {callback} callback
     * @returns {undefined}
     */
    var domReady = function(callback) {
        document.readyState === "interactive" || document.readyState === "complete" ? callback() : document.addEventListener("DOMContentLoaded", callback);
    };

    domReady(function() {
        var template = document.querySelector('.pswp');
        if (template){
            var ps = new Viewer(template);
            ps.init();
        }
    });

})(window, document, Math, PhotoSwipe, PhotoSwipeUI_Default);
