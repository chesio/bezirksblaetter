<?php
/**
 * Backend extensions
 *
 * @package Bezirksblätter
 */

namespace App\Admin;


/**
 * Disable "Administration email verification" feature introduced in WordPress 5.3.
 *
 * @link https://make.wordpress.org/core/2019/10/17/wordpress-5-3-admin-email-verification-screen/
 */
add_filter('admin_email_check_interval', '__return_false', 10, 0);


/**
 * Display number of published media items in "At a Glance" box.
 *
 * @global \wpdb $wpdb
 * @param array $items
 * @return array
 */
add_filter('dashboard_glance_items', function (array $items): array {
    global $wpdb;

    $photos_page = get_option(B13R_MEDIA_PARENT_PAGE, 0);

    if ($photos_page) {
        $media_items_count = intval($wpdb->get_var(
            $wpdb->prepare("SELECT COUNT(ID) as count FROM {$wpdb->posts} WHERE post_type = 'attachment' AND post_parent = %d", $photos_page)
        ));

        $items[] = '<a href="' . admin_url('upload.php') . '" class="media-count">' . sprintf(_n('%d Media Item', '%d Media Items', $media_items_count, 'bezirksblaetter'), $media_items_count) . '</a>';
    }

    return $items;
}, 10, 1);


/**
 * Style the media icon in "At a Glance" box.
 */
add_action('admin_print_styles-index.php', function () {
    echo '<style>#dashboard_right_now li a.media-count:before { content: ""; }</style>';
}, 10, 0);


/**
 * Alter post state info for gallery, tags and media parent pages.
 *
 * @param array $post_states
 * @param \WP_Post $post
 * @return array
 */
add_filter('display_post_states', function (array $post_states, \WP_Post $post): array {
    if ($post->ID === get_option(B13R_GALLERY_PAGE, 0)) {
        $post_states['b13r_gallery_page'] = __('Gallery Page', 'bezirksblaetter');
    }

    if ($post->ID === get_option(B13R_MEDIA_PARENT_PAGE, 0)) {
        $post_states['b13r_media_parent_page'] = __('Media Parent Page', 'bezirksblaetter');
    }

    if ($post->ID === get_option(B13R_TAGS_PAGE, 0)) {
        $post_states['b13r_tags_page'] = __('Tags Page', 'bezirksblaetter');
    }

    return $post_states;
}, 10, 2);


/**
 * Due to buggy implementation, a full size image is loaded in edit attachment screen.
 *
 * @see edit_form_image_editor()
 *
 * @param bool $downsize
 * @param int|null $id
 * @param string|array $size
 * @return array|false
 */
add_filter('image_downsize', function (bool $downsize, ?int $id, $size) {
    return (is_array($size) && (count($size) === 2) && ($size[0] === 900) && ($size[1] === 450))
        ? image_downsize($id, 'medium_large')
        : $downsize
    ;
}, 10, 3);


/**
 * Add meta box with image metadata to attachment edit screen.
 *
 * @param \WP_Post $post
 */
add_action('add_meta_boxes_attachment', function (\WP_Post $post) {
    if (wp_attachment_is('image', $post)) {
        add_meta_box('media-image-metadata', __('Image metadata', 'bezirksblaetter'), __NAMESPACE__ . '\\render_media_image_metadata_meta_box', 'attachment', 'side');
    }
}, 10, 1);


/**
 * Render meta box with attachment image metadata.
 *
 * @param \WP_Post $post
 */
function render_media_image_metadata_meta_box(\WP_Post $post)
{
    $metadata = wp_get_attachment_metadata($post->ID, true);
    $image_meta = is_array($metadata) && isset($metadata['image_meta']) ? $metadata['image_meta'] : null;

    if (is_array($image_meta)) {
        echo '<ul>';
        if (isset($image_meta['created_timestamp'])) {
            echo '<li>' . esc_html__('Created', 'bezirksblaetter') . ': <strong>' . date_i18n('d.m.Y H:i:s', $image_meta['created_timestamp']) . '</strong></li>';
        }
        if (isset($image_meta['camera'])) {
            echo '<li>' . esc_html__('Camera model', 'bezirksblaetter') . ': <strong>' . $image_meta['camera'] . '</strong></li>';
        }
        if (isset($image_meta['aperture'])) {
            echo '<li>' . esc_html__('Aperture', 'bezirksblaetter') . ': <strong>f/' . $image_meta['aperture'] . '</strong></li>';
        }
        if (isset($image_meta['shutter_speed'])) {
            echo '<li>' . esc_html__('Shutter speed', 'bezirksblaetter') . ': <strong>' . sprintf(esc_html__('%s sec', 'bezirksblaetter'), $image_meta['shutter_speed']) . '</strong></li>';
        }
        if (isset($image_meta['focal_length'])) {
            echo '<li>' . esc_html__('Focal length', 'bezirksblaetter') . ': <strong>' . sprintf(esc_html__('%s mm', 'bezirksblaetter'), $image_meta['focal_length']) . '</strong></li>';
        }
        if (isset($image_meta['iso'])) {
            echo '<li>' . esc_html__('ISO speed', 'bezirksblaetter') . ': <strong>' . $image_meta['iso'] . '</strong></li>';
        }
        echo '</ul>';
    } else {
        echo '<p>' . esc_html__('No image metadata present.', 'bezirksblaetter') . '</p>';
    }
}


/**
 * Add meta box with menu order to attachment edit screen.
 */
add_action('add_meta_boxes_attachment', function () {
    add_meta_box('media-menu-order', __('Menu order', 'bezirksblaetter'), __NAMESPACE__ . '\\render_media_menu_order_meta_box', 'attachment', 'side');
}, 10, 0);


/**
 * Render meta box with menu order.
 *
 * @param \WP_Post $post
 */
function render_media_menu_order_meta_box(\WP_Post $post)
{
    // Note: since menu_order is a standard field, it is enough to just add the input field.
    echo '<p><input name="menu_order" type="text" value="' . esc_attr($post->menu_order) . '"></p>';
}
