<?php
/**
 * Enhancements for attachment handling in WordPress core
 *
 * @package Bezirksblätter
 */

namespace App\Attachments;


/**
 * Disable "Big image handling" feature introduced in WordPress 5.3.
 *
 * @link https://make.wordpress.org/core/2019/10/11/updates-to-image-processing-in-wordpress-5-3/
 */
add_filter('big_image_size_threshold', '__return_false', 10, 0);


/**
 * Remove additional image sizes introduced in WordPress 5.3.
 *
 * @todo Maybe incorporate them in PhotoSwipe as additional sizes?
 */
add_action('after_setup_theme', function () {
    remove_image_size('1536x1536');
    remove_image_size('2048x2048');
}, 10, 0);


/**
 * Include attachments in suggestions offered by "Insert link" box.
 *
 * @internal By default attachments are not included because of their `inherit` post status.
 *
 * @param array $results An array of results associative array of query results.
 * @param array $query An array of WP_Query arguments.
 *
 * @return array Filtered array of $results.
 */
add_filter('wp_link_query', function (array $results, array $query): array {

    if (0 === ($media_parent_page = get_option(B13R_MEDIA_PARENT_PAGE, 0))) {
        // Do nothing if media parent page is not set.
        return $results;
    }

    // Grab a hint whether the link is being inserted from media gallery/tag or page/post edit screen.
    $post_id = filter_input(INPUT_POST, 'b13r_post_id', FILTER_VALIDATE_INT);
    $term_id = filter_input(INPUT_POST, 'b13r_term_id', FILTER_VALIDATE_INT);
    $taxonomy = filter_input(INPUT_POST, 'b13r_taxonomy', FILTER_SANITIZE_STRING);
    // The implementation inherently assumes these two can never be both true at the same time.
    $is_post_edit_screen = $post_id !== null;
    $is_term_edit_screen = $term_id !== null && $taxonomy !== '';

    if ($is_term_edit_screen && !empty($query['offset'])) {
        // When fetching results for term edit screen, all results are provided in one go, therefore ignore paging requests.
        return $results;
    }

    $attachment_query = [
        'post_type'         => 'attachment', // Note: proper post_status is set by get_posts() automatically for attachment post type.
        // These are taken verbatim from wp_link_query().
        'suppress_filters'          => true,
        'update_post_term_cache'    => false,
        'update_post_meta_cache'    => false,
        // These are B13R extras:
        'post_parent' => $media_parent_page,
    ];

    // Include search term if provided.
    if (isset($query['s'])) {
        $attachment_query['s'] = $query['s'];
    }

    if ($is_post_edit_screen) {
        // Keep paging working.
        $attachment_query['posts_per_page'] = $query['posts_per_page'] ?? 20;
        $attachment_query['offset'] = $query['offset'] ?? 0;
    }

    if ($is_term_edit_screen) {
        // Limit the results only to attachments assigned the particular media gallery/tag being edited.
        // https://developer.wordpress.org/reference/classes/wp_query/#taxonomy-parameters
        $attachment_query['tax_query'] = [
            [
                'taxonomy'  => $taxonomy,
                'field'     => 'term_id',
                'terms'     => $term_id,
            ],
        ];
        // Fetch all results right away.
        $attachment_query['posts_per_page'] = -1;
    }

    $attachments = get_posts($attachment_query);

    // Build results.
    $attachment_results = [];
    foreach ($attachments as $attachment) {
        // If media gallery or media tag is edited, display link to the image.
        if ($is_term_edit_screen && wp_attachment_is_image($attachment)) {
            // Add link to attachment file.
            $attachment_results[] = [
                'ID'        => $attachment->ID,
                'title'     => trim(esc_html(strip_tags(get_the_title($attachment)))),
                'permalink' => wp_get_attachment_image_url($attachment->ID, 'full'),
                'info'      => __('Photo file', 'bezirksblaetter'),
            ];
        }
        if ($is_post_edit_screen) {
            // Add link to attachment page.
            $attachment_results[] = [
                'ID'        => $attachment->ID,
                'title'     => trim(esc_html(strip_tags(get_the_title($attachment)))),
                'permalink' => get_permalink($attachment),
                'info'      => __('Photo page', 'bezirksblaetter'),
            ];
        }
    }

    // Prefer media-related results over default ones.
    return $attachment_results + $results;
}, 10, 2);


/**
 * Add a signal to `wp-link-ajax` action whether post/page or media gallery/tag is being edited.
 *
 * @internal Implementation inspired by Polylang.
 *
 * @global $post_ID
 * @global $tag_ID
 * @global $taxonomy
 */
add_action('admin_print_footer_scripts', function () {
    global $post_ID, $tag_ID, $taxonomy;

    $params = [];
    if (!empty($post_ID)) {
        $params['b13r_post_id'] = $post_ID;
    }
    if (!empty($tag_ID)) {
        $params['b13r_term_id'] = $tag_ID;
    }
    if (!empty($taxonomy)) {
        $params['b13r_taxonomy'] = $taxonomy;
    }

    if (($query = http_build_query($params)) !== '') {
?>
    <script>
        if (typeof jQuery !== 'undefined') {
            (function($) {
                $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
                    // Inject custom query data only into `wp-link-ajax` requests.
                    if (   typeof originalOptions.data === 'object'
                        && originalOptions.data.hasOwnProperty('action')
                        && originalOptions.data.action === 'wp-link-ajax'
                        && typeof options.data === 'string'
                    ) {
                        options.data += '&<?= $query; ?>';
                    }
                });
            })(jQuery);
        }
    </script>
<?php
    }
}, 1, 0);


/**
 * Make attachments query-able and searchable. Disable paging and adjust ordering on attachment taxonomy pages.
 *
 * @param \WP_Query $query
 * @return \WP_Query
 */
add_filter('pre_get_posts', function (\WP_Query $query): \WP_Query {
    if ($query->is_tax(MEDIA_GALLERIES_TAXONOMY) || $query->is_tax(MEDIA_TAGS_TAXONOMY)) {
        $query->set('post_status', 'any');
        $query->set('posts_per_page', -1);
        $query->set('orderby', 'menu_order');
    }
    if ($query->is_tax(MEDIA_GALLERIES_TAXONOMY)) {
        $query->set('order', 'ASC');
    }
    if ($query->is_tax(MEDIA_TAGS_TAXONOMY)) {
        $query->set('order', 'DESC');
    }
    if ($query->is_search() && !is_admin()) {
        $query->set('post_type', ['post', 'attachment']);
        $query->set('post_status', ['publish', 'inherit']);
    }
    return $query;
}, 10, 1);


/**
 * Do not prepend attachment via the_content filter on taxonomy pages.
 */
add_filter('template_redirect', function () {
    if (is_tax(MEDIA_GALLERIES_TAXONOMY) || is_tax(MEDIA_TAGS_TAXONOMY)) {
        remove_filter('the_content', 'prepend_attachment');
    }
}, 10, 0);


/**
 * Add rel="prev" and image title to previous image link.
 *
 * @param string $output
 * @param int $attachment_id
 * @return string
 */
add_filter('previous_image_link', function (string $output, int $attachment_id): string {
    return str_replace('<a', '<a rel="prev" title="' . esc_attr(get_the_title($attachment_id)) . '"', $output);
}, 10, 2);


/**
 * Add rel="next" and image title to next image link.
 *
 * @param string $output
 * @param int $attachment_id
 * @return string
 */
add_filter('next_image_link', function (string $output, int $attachment_id): string {
    return str_replace('<a', '<a rel="next" title="' . esc_attr(get_the_title($attachment_id)) . '"', $output);
}, 10, 2);


/**
 * Set menu order of attachment to timestamp corresponding to date and time of attachment media file creation.
 *
 * The method to get creation data and time differs for image and video and also in case of insertion and update (for images).
 *
 * @param array $data
 * @param array $postarr
 * @return array
 */
add_filter('wp_insert_attachment_data', function (array $data, array $postarr): array {
    if (strpos($data['post_mime_type'], 'image/') === 0) {
        $image_meta = null;

        if ($postarr['file']) {
            // Path to uploaded file is only provided on attachment insertion (not update):
            $image_meta = wp_read_image_metadata($postarr['file']);
        } elseif ($postarr['ID']) {
            // Attachment metadata are only available on attachment update (not insertion):
            $metadata = wp_get_attachment_metadata($postarr['ID'], true);
            $image_meta = is_array($metadata) && isset($metadata['image_meta']) ? $metadata['image_meta'] : null;
        }

        if (is_array($image_meta) && isset($image_meta['created_timestamp'])) {
            // Set menu order to creation timestamp
            $data['menu_order'] = $image_meta['created_timestamp'];
        }
    } elseif ((strpos($data['post_mime_type'], 'video/') === 0) && isset($postarr['attachment_url'])) {
        // Attempt to get creation date and time from attachment filename
        // Example: VID_20170513_145025_whatever.mp4 --> 13-05-2017 14:50:25
        $pattern = '|^vid_(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})(\d{2})|i';
        $subject = basename($postarr['attachment_url']);
        $matches = [];
        // If menu order is already set, do nothing.
        if (empty($data['menu_order']) && preg_match($pattern, $subject, $matches)) {
            $data['menu_order'] = mktime(
                intval($matches[4]), // hour
                intval($matches[5]), // minute
                intval($matches[6]), // second
                intval($matches[2]), // month
                intval($matches[3]), // day
                intval($matches[1])  // year
            );
        }
    }

    return $data;
}, 10, 2);


/**
 * Set post parent ID of newly inserted attachments to the "Media parent" page.
 */
add_filter('wp_insert_post_parent', function (int $post_parent, int $post_id, array $post_data, array $raw_data): int {
    if ($post_parent !== 0) {
        // Only modify post parent if not set explicitly.
        return $post_parent;
    }
    if ($post_data['post_type'] !== 'attachment') {
        // Only modify post parent of attachments.
        return $post_parent;
    }
    if (empty($file = $raw_data['file'])) {
        // Without filename, there's nothing we can do here...
        return $post_parent;
    }
    if (empty($pattern = apply_filters('bezirksblaetter/media_filename_regex', ''))) {
        // No regex for determination of media files is set by appropriate filter.
        return $post_parent;
    }
    if (preg_match($pattern, basename($file)) !== 1) {
        // Filename does not match expected pattern.
        return $post_parent;
    }

    // Ok, set post parent to media parent page.
    return get_option(B13R_MEDIA_PARENT_PAGE, 0);
}, 10, 4);


/**
 * Include attachments in WordPress XML sitemap - step 1/2.
 *
 * @param array $post_types
 * @return array
 */
add_filter('wp_sitemaps_post_types', function (array $post_types): array {
    // Include (explicitly excluded by default) attachment post type.
    return array_merge($post_types, get_post_types(['name' => 'attachment'], 'objects'));
}, 10, 1);

/**
 * Include attachments in WordPress XML sitemap - step 2/2.
 *
 * @param array $args
 * @param string $post_type
 * @return array
 */
add_filter('wp_sitemaps_posts_query_args', function (array $args, string $post_type): array {
    // Fix query arguments for attachment post type.
    if ($post_type === 'attachment') {
        $args['post_status'] = ['publish', 'inherit']; // Attachments have 'inherit' status.
        $args['post_parent'] = get_option(B13R_MEDIA_PARENT_PAGE, 0); // Only include proper media items.
    }

    return $args;
}, 10, 2);
