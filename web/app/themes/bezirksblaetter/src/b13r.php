<?php
/**
 * Customizations (not generally useful stuff) for bezirksblaetter.cz
 *
 * Normally, this should be included in a child theme, but I'm too lazy now.
 *
 * @package Bezirksblätter
 */

namespace App\Filters;

/**
 * Do not load default Block Editor stylesheet.
 *
 * Unfortunately, Classic Editor does not help here...
 *
 * @link https://wordpress.org/support/topic/block-editor-assets-still-enqueued/
 */

remove_action('wp_enqueue_scripts', 'wp_common_block_scripts_and_styles');


/**
 * Remove author from media table column.
 *
 * @param array $posts_columns
 * @return array
 */
add_filter('manage_media_columns', function (array $posts_columns): array {
    unset($posts_columns['author']);
    return $posts_columns;
}, 10, 1);


// BC Security integration
if (interface_exists(\BlueChip\Security\Modules\Checklist\Hooks::class)) {

    /**
     * Ignore premium plugins in core integrity check.
     *
     * @param array $plugins
     *
     * @return array
     */
    function unsetPremiumPlugins(array $plugins): array
    {
        // Polylang Pro is not hosted in Plugins Directory.
        unset($plugins['polylang-pro/polylang.php']);
        return $plugins;
    }

    /**
     * Ignore premium plugins in core integrity check.
     */
    add_filter(\BlueChip\Security\Modules\Checklist\Hooks::PLUGINS_TO_CHECK_FOR_INTEGRITY, __NAMESPACE__ . '\\unsetPremiumPlugins', 10, 1);

    /**
     * Ignore premium plugins in removed plugins check.
     */
    add_filter(\BlueChip\Security\Modules\Checklist\Hooks::PLUGINS_TO_CHECK_FOR_REMOVAL, __NAMESPACE__ . '\\unsetPremiumPlugins', 10, 1);
}


/**
 * Filter (set) regex for auto-determination of media files. The regex is applied to file's basename.
 *
 * @return string
 */
add_filter('bezirksblaetter/media_filename_regex', function (): string {
    // Recognize following media files:
    // DSC - Sony RX100M2 photo file
    // MAH - Sony RX100M2 video file
    // IMG - Nexus 5X photo file
    // VID - Nexus 5X video file
    return '/^(DSC\d{5}\.jpg|MAH\d{5}\.mp4|IMG_\d{8}_\d{6}\.jpg|VID_\d{8}_\d{6}\.mp4)$/i';
}, 10, 0);
