<?php
/**
 * Stuff that I miss in WordPress core:
 *
 * - adjacent_media_link(): core has only adjacent_image_link() that is limited to post_mime_type = 'image'
 *
 * @package Bezirksblätter
 */


/**
 * Displays previous media link that has the same post parent.
 *
 * @since ?.?.?
 *
 * @see adjacent_media_link()
 *
 * @param string       $text Link text.
 */
function previous_media_link(string $text)
{
    adjacent_media_link($text, true);
}

/**
 * Displays next media link that has the same post parent.
 *
 * @since ?.?.?
 *
 * @see adjacent_media_link()
 *
 * @param string       $text Link text.
 */
function next_media_link(string $text)
{
    adjacent_media_link($text, false);
}

/**
 * Displays next or previous media link that has the same post parent.
 *
 * Retrieves the current attachment object from the $post global.
 *
 * @since ?.?.?
 *
 * @param string       $text Link text.
 * @param bool         $prev Optional. Whether to display the next (false) or previous (true) link. Default true.
 */
function adjacent_media_link(string $text, bool $prev = true)
{
    $post = get_post();
    $attachments = array_values(get_children([
        'post_parent' => $post->post_parent,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
    ]));

    foreach ($attachments as $k => $attachment) {
        if ($attachment->ID === $post->ID) {
            break;
        }
    }

    $output = '';
    $attachment_id = 0;

    if ($attachments) {
        $k = $prev ? $k - 1 : $k + 1;

        if (isset($attachments[$k])) {
            $attachment_id = $attachments[$k]->ID;
            $output = sprintf(
                '<a href="%s" title="%s" rel="%s">%s</a>',
                esc_url(get_attachment_link($attachment_id)),
                esc_attr(get_the_title($attachment_id)),
                $prev ? 'prev' : 'next',
                $text
            );
        }
    }

    $adjacent = $prev ? 'previous' : 'next';

    /**
     * Filters the adjacent media link.
     *
     * The dynamic portion of the hook name, `$adjacent`, refers to the type of adjacency,
     * either 'next', or 'previous'.
     *
     * @since 3.5.0
     *
     * @param string $output        Adjacent media HTML markup.
     * @param int    $attachment_id Attachment ID
     * @param string $size          Image size.
     * @param string $text          Link text.
     */
    echo apply_filters("{$adjacent}_media_link", $output, $attachment_id, $text);
}
