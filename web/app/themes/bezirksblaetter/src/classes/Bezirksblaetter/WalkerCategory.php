<?php
/**
 * @package Bezirksblätter
 */

namespace CeP\Bezirksblaetter;


/**
 * Class extends default Walker_Category in the following way:
 * 1) Padded counts are displayed if `show_count` is true.
 * 2) Padded counts are properly computed for attachments (posts with post status "inherit").
 * 3) Category description is displayed after category name, if `use_desc_for_title` is false.
 */
class WalkerCategory extends \Walker_Category {

    /** @var array */
    private $terms = [];


    /**
     * @param array $terms
     * @param string $taxonomy
     */
    public function __construct($terms, $taxonomy) {
        // Compute padded counts
        $this->_pad_term_counts($terms, $taxonomy);
        // Store terms with updated counts
        foreach ($terms as $term) {
            $this->terms[$term->term_id] = $term;
        }
    }


    /**
     * Starts the element output.
     *
     * @since 2.1.0
     * @access public
     *
     * @see Walker::start_el()
     *
     * @param string $output   Passed by reference. Used to append additional content.
     * @param object $category Category data object.
     * @param int    $depth    Optional. Depth of category in reference to parents. Default 0.
     * @param array  $args     Optional. An array of arguments. See wp_list_categories(). Default empty array.
     * @param int    $id       Optional. ID of the current category. Default 0.
     */
    public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
        /** This filter is documented in wp-includes/category-template.php */
        $cat_name = apply_filters(
            'list_cats',
            esc_attr( $category->name ),
            $category
        );

        // Don't generate an element if the category name is empty.
        if ( ! $cat_name ) {
            return;
        }

        $link = '<a href="' . esc_url( get_term_link( $category ) ) . '" ';
        if ( $args['use_desc_for_title'] && ! empty( $category->description ) ) {
            /**
             * Filters the category description for display.
             *
             * @since 1.2.0
             *
             * @param string $description Category description.
             * @param object $category    Category object.
             */
            $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
        }

        $link .= '>';
        $link .= $cat_name . '</a>';

        if ( ! empty( $args['feed_image'] ) || ! empty( $args['feed'] ) ) {
            $link .= ' ';

            if ( empty( $args['feed_image'] ) ) {
                $link .= '(';
            }

            $link .= '<a href="' . esc_url( get_term_feed_link( $category->term_id, $category->taxonomy, $args['feed_type'] ) ) . '"';

            if ( empty( $args['feed'] ) ) {
                $alt = ' alt="' . sprintf(__( 'Feed for all posts filed under %s' ), $cat_name ) . '"';
            } else {
                $alt = ' alt="' . $args['feed'] . '"';
                $name = $args['feed'];
                $link .= empty( $args['title'] ) ? '' : $args['title'];
            }

            $link .= '>';

            if ( empty( $args['feed_image'] ) ) {
                $link .= $name;
            } else {
                $link .= "<img src='" . $args['feed_image'] . "'$alt" . ' />';
            }
            $link .= '</a>';

            if ( empty( $args['feed_image'] ) ) {
                $link .= ')';
            }
        }

        if ( ! empty( $args['show_count'] ) ) {
            $link .= ' (' . number_format_i18n( isset($this->terms[$category->term_id]) ? $this->terms[$category->term_id]->count : $category->count ) . ')'; // CeP
        }

        // CeP: start
        if ( ! $args['use_desc_for_title'] && ! empty( $category->description ) ) {
            $link .= ' &mdash; ' . $category->description;
        }
        // CeP: end

        if ( 'list' == $args['style'] ) {
            $output .= "\t<li";
            $css_classes = array(
                'cat-item',
                'cat-item-' . $category->term_id,
            );

            if ( ! empty( $args['current_category'] ) ) {
                // 'current_category' can be an array, so we use `get_terms()`.
                $_current_terms = get_terms( $category->taxonomy, array(
                    'include' => $args['current_category'],
                    'hide_empty' => false,
                ) );

                foreach ( $_current_terms as $_current_term ) {
                    if ( $category->term_id == $_current_term->term_id ) {
                        $css_classes[] = 'current-cat';
                    } elseif ( $category->term_id == $_current_term->parent ) {
                        $css_classes[] = 'current-cat-parent';
                    }
                    while ( $_current_term->parent ) {
                        if ( $category->term_id == $_current_term->parent ) {
                            $css_classes[] =  'current-cat-ancestor';
                            break;
                        }
                        $_current_term = get_term( $_current_term->parent, $category->taxonomy );
                    }
                }
            }

            /**
             * Filters the list of CSS classes to include with each category in the list.
             *
             * @since 4.2.0
             *
             * @see wp_list_categories()
             *
             * @param array  $css_classes An array of CSS classes to be applied to each list item.
             * @param object $category    Category data object.
             * @param int    $depth       Depth of page, used for padding.
             * @param array  $args        An array of wp_list_categories() arguments.
             */
            $css_classes = implode( ' ', apply_filters( 'category_css_class', $css_classes, $category, $depth, $args ) );

            $output .=  ' class="' . $css_classes . '"';
            $output .= ">$link\n";
        } elseif ( isset( $args['separator'] ) ) {
            $output .= "\t$link" . $args['separator'] . "\n";
        } else {
            $output .= "\t$link<br />\n";
        }
    }


    /**
     * Add count of children to parent count.
     *
     * Recalculates term counts by including items from child terms. Assumes all
     * relevant children are already in the $terms argument.
     *
     * @see _pad_term_counts Legacy method (that is broken for attachments)
     *
     * @global wpdb $wpdb WordPress database abstraction object.
     *
     * @param array  $terms    List of term objects, passed by reference.
     * @param string $taxonomy Term context.
     */
    private function _pad_term_counts( &$terms, $taxonomy ) {
        global $wpdb;

        // This function only works for hierarchical taxonomies like post categories.
        if ( !is_taxonomy_hierarchical( $taxonomy ) )
            return;

        $term_hier = _get_term_hierarchy($taxonomy);

        if ( empty($term_hier) )
            return;

        $term_items = array();
        $terms_by_id = array();
        $term_ids = array();

        foreach ( (array) $terms as $key => $term ) {
            $terms_by_id[$term->term_id] = & $terms[$key];
            $term_ids[$term->term_taxonomy_id] = $term->term_id;
        }

        // Get the object and term ids and stick them in a lookup table.
        $tax_obj = get_taxonomy($taxonomy);
        $object_types = esc_sql($tax_obj->object_type);
        $results = $wpdb->get_results("SELECT object_id, term_taxonomy_id FROM $wpdb->term_relationships INNER JOIN $wpdb->posts ON object_id = ID WHERE term_taxonomy_id IN (" . implode(',', array_keys($term_ids)) . ") AND post_type IN ('" . implode("', '", $object_types) . "') AND (post_status = 'publish' OR post_status = 'inherit')");
        foreach ( $results as $row ) {
            $id = $term_ids[$row->term_taxonomy_id];
            $term_items[$id][$row->object_id] = isset($term_items[$id][$row->object_id]) ? ++$term_items[$id][$row->object_id] : 1;
        }

        // Touch every ancestor's lookup row for each post in each term.
        foreach ( $term_ids as $term_id ) {
            $child = $term_id;
            $ancestors = array();
            while ( !empty( $terms_by_id[$child] ) && $parent = $terms_by_id[$child]->parent ) {
                $ancestors[] = $child;
                if ( !empty( $term_items[$term_id] ) )
                    foreach ( $term_items[$term_id] as $item_id => $touches ) {
                        $term_items[$parent][$item_id] = isset($term_items[$parent][$item_id]) ? ++$term_items[$parent][$item_id]: 1;
                    }
                $child = $parent;

                if ( in_array( $parent, $ancestors ) ) {
                    break;
                }
            }
        }

        // Transfer the touched cells.
        foreach ( (array) $term_items as $id => $items )
            if ( isset($terms_by_id[$id]) )
                $terms_by_id[$id]->count = count($items);
    }

}
