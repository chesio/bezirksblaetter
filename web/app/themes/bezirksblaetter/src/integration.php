<?php
/**
 * Integration with particluar 3rd party plugins:
 * - Nginx Cache: https://wordpress.org/plugins/nginx-cache/
 * - Polylang Pro: https://polylang.pro/
 * - Statify: https://wordpress.org/plugins/statify/
 * - Wordfence Login Security: https://wordpress.org/plugins/wordfence-login-security/
 *
 * @package Bezirksblätter
 */

namespace App\Integration;

/**
 * Do not warn about files that have been patched.
 *
 * @see `/patches` directory
 */
if (class_exists(\BlueChip\Security\Plugin::class)) { // Note: check Plugin class, because Hooks class might not be autoloaded yet.
    add_filter(\BlueChip\Security\Modules\Checklist\Hooks::IGNORED_CORE_MODIFIED_FILES, function (array $ignored_files): array {
        return array_merge($ignored_files, ['wp-includes/class-wp-http.php',  'wp-includes/link-template.php']);
    }, 10, 1);
}


/**
 * Flush Nginx cache ...
 */
add_filter('nginx_cache_purge_actions', function (array $actions): array {
    // ...when post category or post tag is edited.
    $actions[] = 'edit_category';
    $actions[] = 'edit_post_tag';
    // ...when media gallery or media tag is edited.
    $actions[] = 'edit_' . MEDIA_GALLERIES_TAXONOMY;
    $actions[] = 'edit_' . MEDIA_TAGS_TAXONOMY;
    // ...when media item is added or edited.
    $actions[] = 'add_attachment';
    $actions[] = 'edit_attachment';

    return $actions;
}, 10, 1);


/**
 * Polylang: Sync media galleries and media tags taxonomies across translations.
 *
 * @link https://polylang.pro/doc/filter-reference/
 *
 * @param array $taxonomies
 *
 * @return array
 */
add_filter('pll_copy_taxonomies', function (array $taxonomies): array {
    $taxonomies[] = MEDIA_GALLERIES_TAXONOMY;
    $taxonomies[] = MEDIA_TAGS_TAXONOMY;
    return $taxonomies;
}, 10, 1);


if (function_exists('pll_get_post')) {
    /**
     * Polylang: Translate page ID of pages set via options, if possible.
     *
     * @param int $value
     *
     * @return int
     */
    function translate_option(int $value): int
    {
        $translated_id = pll_get_post($value);
        // If no current language is defined or page has no translation, return original page ID.
        return intval($translated_id ?: $value);
    }
    add_filter('option_' . B13R_GALLERY_PAGE, __NAMESPACE__ . '\\translate_option');
    add_filter('option_' . B13R_MEDIA_PARENT_PAGE, __NAMESPACE__ . '\\translate_option');
    add_filter('option_' . B13R_TAGS_PAGE, __NAMESPACE__ . '\\translate_option');

    /**
     * Polylang: translate post parent ID if used in XML sitemap post query args.
     *
     * @param array $args
     * @return array
     */
    add_filter('wp_sitemaps_posts_query_args', function (array $args): array {
        if (isset($args['post_parent']) && isset($args['lang'])) {
            $translated_post_parent = pll_get_post($args['post_parent'], $args['lang']);
            $args['post_parent'] = intval($translated_post_parent ?: $args['post_parent']);
        }

        return $args;
    }, 100, 1); // Run at the end after all filters are executed!
}


if (function_exists('pll_get_post_translations')) {
    /**
     * Alter post state info for gallery, tags and media parent pages.
     *
     * @param array $post_states
     * @param \WP_Post $post
     *
     * @return array
     */
    add_filter('display_post_states', function (array $post_states, \WP_Post $post): array {
        if (PLL()->curlang) {
            // Nothing to be done here, option filter above does its job.
            return $post_states;
        }

        // Fix the case when "All languages" are selected in backend:

        $gallery_page = get_option(B13R_GALLERY_PAGE, 0);
        if ($gallery_page && in_array($post->ID, pll_get_post_translations($gallery_page), true)) {
            $post_states['b13r_gallery_page'] = __('Gallery Page', 'bezirksblaetter');
        }

        $post_page = get_option(B13R_MEDIA_PARENT_PAGE, 0);
        if ($post_page && in_array($post->ID, pll_get_post_translations($post_page), true)) {
            $post_states['b13r_media_parent_page'] = __('Media Parent Page', 'bezirksblaetter');
        }

        $tags_page = get_option(B13R_TAGS_PAGE, 0);
        if ($tags_page && in_array($post->ID, pll_get_post_translations($tags_page), true)) {
            $post_states['b13r_tags_page'] = __('Tags Page', 'bezirksblaetter');
        }

        return $post_states;
    }, 20, 2);


    /**
     * Update attachment metadata for all existing attachment translations.
     *
     * @param int $meta_id
     * @param int $object_id
     * @param int $meta_key
     * @param mixed $meta_value
     */
    add_action('updated_post_meta', function (int $meta_id, int $object_id, string $meta_key, $meta_value) {
        // Store updated values to avoid infinite loop.
        static $updated = [];

        if ($meta_key === '_wp_attachment_metadata') {
            array_push($updated, $object_id);
            // Process all translations.
            $translations = pll_get_post_translations($object_id);
            foreach ($translations as $translation_id) {
                // Only update translations that has not been processed yet.
                if (!in_array($translation_id, $updated, true)) {
                    update_post_meta($translation_id, $meta_key, $meta_value);
                    array_push($updated, $translation_id);
                }
            }
        }
    }, 10, 4);
}


if (function_exists('pll_current_language') && function_exists('pll_translate_string')) {
    /**
     * Translate page slug in canonical URLs.
     *
     * @param string $canonical_url
     *
     * @return string
     */
    add_filter('get_canonical_url', function (string $canonical_url): string {
        // Get translated page slug.
        $slug = pll_translate_string('page', pll_current_language());

        // Replace /page/ with translation.
        return str_replace('/page/', "/{$slug}/", $canonical_url);
    }, 20, 1);
}


/**
 * Ensure the admin bar is rendered in user's language (snippet 1 of 2).
 *
 * @link https://github.com/polylang/polylang/issues/201#issuecomment-348178243
 *
 * @requires WordPress 4.7
 */
add_action('admin_bar_menu', function () {
    switch_to_locale(get_user_locale());
}, 0, 0);

/**
 * Ensure the admin bar is rendered in user's language (snippet 2 of 2).
 *
 * @link https://github.com/polylang/polylang/issues/201#issuecomment-348178243
 *
 * @requires WordPress 4.7
 */
add_action('admin_bar_menu', function () {
    restore_previous_locale();
}, 100, 0);


/**
 * Skip Statify tracking for configured IP addresses.
 *
 * @param bool|null $skip
 *
 * @return bool|null
 */
if (defined('STATIFY_IGNORE_REMOTE_ADDRESSES')) {
    add_filter('statify__skip_tracking', function (?bool $skip): ?bool {
        return in_array($_SERVER['REMOTE_ADDR'], STATIFY_IGNORE_REMOTE_ADDRESSES, true) ? true : $skip;
    }, 10, 1);
}


/**
 * Move Wordfence Login Security menu from main menu to Settings submenu.
 */
if (class_exists(\WordfenceLS\Controller_Permissions::class) && class_exists(\WordfenceLS\Controller_WordfenceLS::class)) {
    add_action('admin_menu', function () {
        remove_menu_page('WFLS');
        add_options_page(
            __('Login Security', 'wordfence-2fa'),
            __('Login Security', 'wordfence-2fa'),
            \WordfenceLS\Controller_Permissions::CAP_ACTIVATE_2FA_SELF,
            'WFLS',
            [\WordfenceLS\Controller_WordfenceLS::shared(), '_menu']
        );
    }, 100, 0);
}
