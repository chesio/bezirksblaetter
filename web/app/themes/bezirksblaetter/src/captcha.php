<?php
/**
 * Add math captcha to comment form.
 *
 * @package Bezirksblätter
 * @requires WordPress 4.4
 * @todo Move to a plugin.
 */

namespace CeP\Bezirksblaetter\Captcha;


define('B13R_CAPTCHA', true); // Captcha is on

// Field names
define('B13R_CAPTCHA_RESULT', 'b13r_captcha_result');
define('B13R_CAPTCHA_TIMESTAMP', 'b13r_captcha_timestamp');
define('B13R_CAPTCHA_CHECKSUM', 'b13r_captcha_checksum');


/**
 * Generate random math question.
 *
 * @return array
 */
function generate_math_question(): array
{
    $operations = [
        '+' => function ($a, $b) { return $a + $b; },
        '-' => function ($a, $b) { return $a - $b; },
        '×' => function ($a, $b) { return $a * $b; },
    ];

    // Get random operands
    $l_op = rand(1, 20);
    $r_op = rand(1, 20);
    // Get random operation
    $op = array_rand($operations);

    if (($op === '-') && ($l_op < $r_op)) {
        // Prevent negative results by subtraction
        list($l_op, $r_op) = [$r_op, $l_op];
    } elseif (($op === '×') && ($l_op > 10) && ($r_op > 10)) {
        // Prevent multiplication of two numbers larger than 10
        $r_op = rand(1, 10);
    }

    $result = $operations[$op]($l_op, $r_op);

    return [
        'left-operand' => $l_op,
        'operation' => $op,
        'right-operand' => $r_op,
        'result' => $result,
    ];
}


/**
 * Generate checksum for given $timestamp and $result values. Uses wp_hash() internally to make checksum site specific.
 *
 * @see wp_hash()
 *
 * @param string $timestamp
 * @param string $result
 * @return string
 */
function generate_checksum(string $timestamp, string $result): string
{
    return wp_hash($timestamp . '|' . $result, 'nonce');
}


/**
 * Add captcha fields to comment form fields.
 *
 * @param array $comment_fields
 * @return array
 */
function comment_form_fields(array $comment_fields): array
{
    // Get captcha components
    $question = generate_math_question();
    $timestamp = time();
    $checksum = generate_checksum((string) $timestamp, (string) $question['result']);

    // Add captcha input field
    // Note: autocomplete="off" prevents browser from restoring inserted value
    // when returning back to the form via back action.
    $comment_fields[B13R_CAPTCHA_RESULT]
        = '<p class="comment-form-captcha">'
        . '<label for="' . B13R_CAPTCHA_RESULT . '">'
        . esc_html__("Prove that you're an intelligent human.", 'bezirksblaetter')
        . ' <span class="required">*</span>'
        . '</label>'
        . '<input type="text" id="' . B13R_CAPTCHA_RESULT . '" name="' . B13R_CAPTCHA_RESULT . '" value="" autocomplete="off" maxlength="4" aria-required="true" required="required">'
        . '<span class="do-your-math">' . sprintf('= %s %s %s', $question['left-operand'], $question['operation'], $question['right-operand']) . '</span>'
        . '</p>'
    ;
    // Add (hidden) timestamp field
    $comment_fields[B13R_CAPTCHA_TIMESTAMP] = '<input type="hidden" name="' . B13R_CAPTCHA_TIMESTAMP . '" value="' . $timestamp . '">';
    // Add (hidden) checksum field
    $comment_fields[B13R_CAPTCHA_CHECKSUM] = '<input type="hidden" name="' . B13R_CAPTCHA_CHECKSUM . '" value="' . $checksum . '">';

    return $comment_fields;
}
add_filter('comment_form_fields', __NAMESPACE__ . '\\comment_form_fields');


/**
 * Check POST-ed match captcha on comment submission.
 *
 * Function is hooked into `pre_comment_on_post` hook - this is not optimal,
 * because this hook is executed before default fields are checked, but there
 * is no better hook in wp_handle_comment_submission() in the moment.
 *
 * Note: the check is skipped for any logged in user.
 */
function pre_comment_on_post()
{
    if (get_current_user_id()) {
        // Do not check captcha for logged in users as it makes no sense (and they don't see the captcha at all).
        return;
    }

    // Grab captcha data
    $result = filter_input(INPUT_POST, B13R_CAPTCHA_RESULT, FILTER_VALIDATE_INT);
    $timestamp = filter_input(INPUT_POST, B13R_CAPTCHA_TIMESTAMP, FILTER_VALIDATE_INT);
    $checksum = filter_input(INPUT_POST, B13R_CAPTCHA_CHECKSUM);

    if (!is_int($result) || !is_int($timestamp) || !$checksum) {
        wp_die('<p>' . esc_html__("You seem to miss an opportunity to prove that you're an intelligent human. Go back and try again!", 'bezirksblaetter') . '</p>', __('Comment Submission Failure'), ['response' => 200, 'back_link' => true]);
    }

    if ($checksum !== generate_checksum($timestamp, $result)) {
        wp_die('<p>' . esc_html__("You failed to prove that you're an intelligent human! Care to try again?", 'bezirksblaetter') . '</p>', __('Comment Submission Failure'), ['response' => 200, 'back_link' => true]);
    }

    // By default, captcha answer expires in one hour, but captcha time to live can be filtered.
    // Note: expiration is necessary to prevent reuse of captcha data by spambots.
    $expiration_time = $timestamp + apply_filters('bezirksblaetter/captcha_ttl', HOUR_IN_SECONDS);
    if ($expiration_time < time()) {
        // Captcha is too old
        wp_die('<p>' . esc_html__("Sorry, it took you too long to prove that you're an intelligent human.", 'bezirksblaetter') . '</p>', __('Comment Submission Failure'), ['response' => 200, 'back_link' => true]);
    }

    // Fine, captcha is valid, do nothing :)
}
add_action('pre_comment_on_post', __NAMESPACE__ . '\\pre_comment_on_post', 10, 0);
