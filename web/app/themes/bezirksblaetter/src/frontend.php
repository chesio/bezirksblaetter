<?php
/**
 * Filter front-end output.
 *
 * @package Bezirksblätter
 */

namespace App\Frontend;


/**
 * Add <body> classes.
 *
 * @global \WP_Query $wp_query
 * @global \WP_Post $post
 * @param array $classes
 *
 * @return array
 */
add_filter('body_class', function (array $classes): array {
    global $wp_query;
    global $post;

    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    if (is_front_page()) {
        $classes[] = 'front-page';
    }

    if (is_single() && has_post_thumbnail()) {
        $classes[] = 'image-header-on';
    }

    if (is_404() && ($post instanceof \WP_Post)) {
        $classes[] = 'image-header-on';
    }

    if (is_attachment()) {
        $classes[] = 'image-header-on';

        $queried_object = $wp_query->get_queried_object();
        if (\App\isImage($queried_object)) {
            $classes[] = 'attachment-image';
        } else if (\App\isVideo($queried_object)) {
            $classes[] = 'attachment-video';
        }
    }

    return $classes;
}, 10, 1);


/**
 * Clean up the_excerpt().
 *
 * @return string
 */
add_filter('excerpt_more', function (): string {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'bezirksblaetter') . '</a>';
}, 10, 0);


/**
 * Insert SVG icon after "Reply" text in comments template.
 *
 * @param string $link Full "reply-to" link body.
 *
 * @return string
 */
add_filter('comment_reply_link', function (string $link): string {
    $matches = [];
    if (preg_match('#(.*<a[^<>]+>)([^<>]+)(</a>.*)#', $link, $matches)) {
        return $matches[1] . \App\contentWithSvgIcon($matches[2], '', 'mail-reply') . $matches[3];
    }
    return $link;
}, 10, 1);


/**
 * Filter (replace) output of built-in `post_gallery` shortcode.
 *
 * @param string $output
 * @param array $attr
 * @param string $instance
 *
 * @return string
 */
add_filter('post_gallery', function (string $output, array $attr, string $instance): string {
    $gallery = 'gallery-' . $instance;

    $ids = array_filter(array_map('intval', explode(',', $attr['ids'])));

    $photos = get_posts([
        'post_type' => 'attachment',
        'numberposts' => -1,
        'post__in' => $ids,
    ]);

    if (count($photos)) {
        // Enqueue PhotoSwipe and media grid scripts
        wp_enqueue_script('photoswipe');
        wp_enqueue_script('media-grid');
        // Render photos via media grid
        return \App\mediaGrid($gallery, $photos, $attr['size']);
    } else {
        return '';
    }
}, 10, 3);
