<?php
/**
 * Template helpers.
 *
 * @package Bezirksblätter
 */

namespace App;


/**
 * Return page title. Title depends on main query type.
 *
 * @return string
 */
function pageTitle(): string
{
    if (is_home()) {
        if (get_option('page_for_posts', true)) {
            return get_the_title(get_option('page_for_posts', true));
        } else {
            return __('Latest posts', 'bezirksblaetter');
        }
    } elseif (is_author()) {
        return get_the_author_meta('display_name');
    } elseif (is_category() || is_tag() || is_tax()) {
        // Do not prepend "Taxonomy name:" to title, just print term title.
        return single_term_title('', false);
    } elseif (is_archive()) {
        return get_the_archive_title();
    } elseif (is_search()) {
        return sprintf(__('Search results for "%s"', 'bezirksblaetter'), get_search_query());
    } elseif (is_404()) {
        return __('Not Found', 'bezirksblaetter');
    } elseif (is_singular()) {
        return get_the_title();
    }

    // This should not happen...
    return '...';
}


/**
 * Return <a> element linking to given $gallery. Optionally pass ID of SVG icons to be placed before/after gallery name.
 *
 * @param \WP_Term $gallery
 * @param string $rel
 * @param string $icon_before
 * @param string $icon_after
 *
 * @return string
 */
function galleryLink(\WP_Term $gallery, string $rel, string $icon_before = '', string $icon_after = ''): string
{
    return sprintf(
        '<a href="%s" title="%s" rel="%s">%s<span class="label">%s</span>%s</a>',
        get_term_link($gallery),
        esc_attr($gallery->description),
        esc_attr($rel),
        $icon_before ? svgUseIcon($icon_before) : '',
        esc_html($gallery->name),
        $icon_after ? svgUseIcon($icon_after) : ''
    );
}


/**
 * Returns an array of breadcrumb menu items for given $parent_term_id (last breadcrumb item) and (optional) $root_page_id (first breadcrumb item).
 *
 * @param int $parent_term_id
 * @parem int $root_page_id
 *
 * @return array
 */
function breadcrumbMenu(int $parent_term_id, int $root_page_id = 0): array
{
    $menu = [];

    while ($parent_term_id) {

        $term = get_term_by('term_taxonomy_id', $parent_term_id);

        array_push($menu, [
            'href' => get_term_link($term),
            'title' => $term->description,
            'name' => $term->name,
        ]);

        $parent_term_id = $term->parent;
    }

    // Root page
    if ($root_page_id) {
        array_push($menu, [
            'href' => get_permalink($root_page_id),
            'title' => '',
            'name' => get_the_title($root_page_id),
        ]);
    }

    return array_reverse($menu);
}


/**
 * @global \WP_Query $wp_query
 *
 * @return array
 */
function contextualMenu(): array
{
    global $wp_query;

    $items = [];
    if (is_attachment()) {
        $class = 'plain';
    } elseif (is_tax(MEDIA_GALLERIES_TAXONOMY)) {
        $class = 'hierarchical';
        $items = breadcrumbMenu($wp_query->get_queried_object()->parent, get_option(B13R_GALLERY_PAGE, 0));
    } elseif (is_tax(MEDIA_TAGS_TAXONOMY)) {
        $class = 'hierarchical';
        $items = breadcrumbMenu($wp_query->get_queried_object()->parent, get_option(B13R_TAGS_PAGE, 0));
    } else if (is_category()) {
        $class = 'hierarchical';
        $items = breadcrumbMenu($wp_query->get_queried_object()->parent, get_option('page_for_posts', 0));
    } else if (is_singular('post')) {
        $class = 'hierarchical';
        $categories = wp_get_post_terms($wp_query->get_queried_object()->ID, 'category');
        $term_id = $categories ? $categories[0]->term_id : 0; // Assume that first category is the main category
        $items = breadcrumbMenu($term_id, get_option('page_for_posts', 0));
    } else {
        $class = 'flat';
        if (0 !== ($gallery_page_id = get_option(B13R_GALLERY_PAGE, 0))) {
            $items[] = [
                'href' => get_permalink($gallery_page_id),
                'name' => get_the_title($gallery_page_id),
                'title' => '',
            ];
        }
        if (0 !== ($tags_page_id = get_option(B13R_TAGS_PAGE, 0))) {
            $items[] = [
                'href' => get_permalink($tags_page_id),
                'name' => get_the_title($tags_page_id),
                'title' => '',
            ];
        }
        if (0 !== ($posts_page_id = get_option('page_for_posts', 0))) {
            $items[] = [
                'href' => get_permalink($posts_page_id),
                'name' => get_the_title($posts_page_id),
                'title' => '',
            ];
        }
    }

    // Every contextual menu starts with a link to home page
    array_unshift($items, ['class' => 'home', 'href' => home_url('/'), 'rel' => 'home', 'title' => get_option('blogname'), 'content' => svgUseIcon('camera', true) . svgUseIcon('camera-outline', true)]);

    return [ 'class' => $class, 'items' => $items, ];
}


/**
 * Produce HTML for media gallery grid. Gallery is printed with Schema.org markup.
 *
 * @param string $gallery
 * @param array $attachments
 * @param string $size
 *
 * @return string
 */
function mediaGrid(string $gallery, array $attachments, string $size = 'medium'): string
{
    $output = '<div class="media-grid content" data-gallery="' . $gallery . '" itemscope itemtype="http://schema.org/CollectionPage">';

    // Add column markup for Colcade.
    $output .= '<div class="grid-column grid-column--1"></div>';
    $output .= '<div class="grid-column grid-column--2"></div>';
    $output .= '<div class="grid-column grid-column--3"></div>';

    // Add item index (necessary for Colcade + PhotoSwipe integration)
    $image_index = 0;

    foreach ($attachments as $attachment) {

        if (!isImage($attachment) && !isVideo($attachment)) {
            // Only images and videos are supported in the moment
            continue;
        }

        $attachment_id = $attachment->ID;
        $attachment_link = get_attachment_link($attachment);
        $attachment_title = get_the_title($attachment);
        $attachment_caption = wp_get_attachment_caption($attachment_id);
        $attachment_metadata = wp_get_attachment_metadata($attachment_id);

        if (isImage($attachment)) {
            $output
                .= '<div class="grid-item pswp-item" data-item-index="' . $image_index++ . '">'
                . '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">'
                . getPhotoSwipeLink($attachment_id, wp_get_attachment_image($attachment_id, $size), ['title' => $attachment_title])
                . '<a class="pswp-detail info-link" href="' . $attachment_link . '" title="' . esc_attr__('More about photo', 'bezirksblaetter') . '">' . svgUseIcon('info-large') . '</a>'
                . '<meta itemprop="thumbnailUrl" content="' . wp_get_attachment_image_url($attachment_id, $size) . '">'
            ;
        } else {
            $attachment_thumbnail = get_the_post_thumbnail_url($attachment_id);
            $output
                .= '<div class="grid-item">'
                . '<figure class="responsive-embed ' . getVideoRatio($attachment_metadata['width'], $attachment_metadata['height']) . '" itemprop="associatedMedia" itemscope itemtype="http://schema.org/VideoObject">'
                . '<video src="' . wp_get_attachment_url($attachment_id) . '" itemprop="contentUrl" controls poster="' . ($attachment_thumbnail ?: '') . '"></video>'
                . '<a class="info-link" href="' . $attachment_link . '" title="' . esc_attr__('More about video', 'bezirksblaetter') . '">' . svgUseIcon('info-large') . '</a>'
            ;
        }

        $output
            .= '<meta itemprop="name" content="' . esc_attr($attachment_title) . '">'
            . '<meta itemprop="contentUrl" content="' . wp_get_attachment_url($attachment_id) . '">'
            . '<meta itemprop="width" content="' . $attachment_metadata['width'] . '">'
            . '<meta itemprop="height" content="' . $attachment_metadata['height'] . '">'
            . ($attachment_caption ? ('<figcaption itemprop="caption description">' . esc_html($attachment_caption) . '</figcaption>') : '')
            . '</figure>'
            . '</div>'
        ;
    }

    $output .= '</div>';

    return $output;
}


/**
 * Return PhotoSwipe ID for given $url.
 *
 * @param string $url
 *
 * @return string
 */
function getPhotoSwipeId(string $url): string
{
    return strtolower(basename($url));
}


/**
 * Return an array with all parameters for construction of PhotoSwipe powered link.
 *
 * @param int $attachment_id
 *
 * @return array
 */
function getPhotoSwipeLinkAttributes(int $attachment_id): array
{
    $image_title = get_the_title($attachment_id);
    $image_attrs_full = wp_get_attachment_image_src($attachment_id, 'full');
    $image_attrs_large = wp_get_attachment_image_src($attachment_id, 'large');
    $image_attrs_medium = wp_get_attachment_image_src($attachment_id, 'medium_large');

    return [
        'class' => 'pswp-link',
        'data-title' => esc_attr($image_title),
        'data-pid' => getPhotoSwipeId($image_attrs_full[0]),
        'href' => esc_url($image_attrs_full[0]),
        'data-width' => $image_attrs_full[1],
        'data-height' => $image_attrs_full[2],
        'data-large-url' => esc_url($image_attrs_large[0]),
        'data-large-width' => $image_attrs_large[1],
        'data-large-height' => $image_attrs_large[2],
        'data-medium-url' => esc_url($image_attrs_medium[0]),
        'data-medium-width' => $image_attrs_medium[1],
        'data-medium-height' => $image_attrs_medium[2],
    ];
}


/**
 * Wrap $content in PhotoSwipe powered link to image file under $attachment_id.
 *
 * @param int $attachment_id Attachment ID of image the <a> element should link to.
 * @param string $content Content to be wrapped in <a> element.
 * @param array $attributes (optional) Additional attributes for <a> element.
 *
 * @return string
 */
function getPhotoSwipeLink(int $attachment_id, string $content, array $attributes = []): string
{
    $atts = array_merge(getPhotoSwipeLinkAttributes($attachment_id), $attributes);

    // <a ...>
    $output = '<a ';
    foreach ($atts as $key => $value) {
        $output .= sprintf(' %s="%s"', $key, $value);
    }
    $output .= '>';
    // <img> or text content
    $output .= $content;
    // </a>
    $output .= '</a>';

    return $output;
}


/**
 * Return (class name for) aspect ratio for video with given $width and $height.
 *
 * @static array $ratios
 *
 * @param int $width
 * @param int $height
 * @param float $epsilon
 *
 * @return string
 */
function getVideoRatio(int $width, int $height, float $epsilon = 0.01): string
{
    static $ratios = [
        'widescreen' => [
            'w' => 16,
            'h' => 9,
        ],
        'vertical' => [
            'w' => 9,
            'h' => 16,
        ],
    ];

    foreach ($ratios as $ratio => $sizes) {
        if (($width / $sizes['w'] - $height / $sizes['h']) < $epsilon) {
            return $ratio;
        }
    }

    return 'default';
}


/**
 * Return <use /> tag with xlink:href referencing given $icon wrapped in <svg class="icon" /> tag.
 *
 * @param string $icon ID of SVG icon.
 * @param bool|string $class Extra class to be added to <svg /> as "icon-$class". If true, ID of SVG icon is used.
 *
 * @return string
 */
function svgUseIcon(string $icon, $class = false): string
{
    return sprintf(
        '<svg class="icon %s"><use xlink:href="%s#%s" /></svg>',
        $class ? sprintf("icon-%s", is_bool($class) ? $icon : $class) : '',
        getAssetUri('icons.svg'),
        $icon
    );
}


/**
 * @param string $content Content - can be HTML, but will be wrapped in <span/> element, so should be an inline element.
 * @param string $icon_before
 * @param string $icon_after
 *
 * @return string
 */
function contentWithSvgIcon(string $content, string $icon_before = '', string $icon_after = ''): string
{
    return ($icon_before ? svgUseIcon($icon_before) : '')
        . '<span class="label">' . $content . '</span>'
        . ($icon_after ? svgUseIcon($icon_after) : '')
    ;
}
