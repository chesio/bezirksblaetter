<?php
/**
 * Helper functions
 *
 * @package Bezirksblätter
 */

namespace App;

use CeP\Bezirksblaetter\Template;


/**
 * @param string $layout
 *
 * @return \CeP\Bezirksblaetter\Template
 */
function getTemplate(string $layout = 'base'): Template
{
    return Template::$instances[$layout];
}


/**
 * @param string $template
 * @param array $context
 * @param string $layout
 */
function includeTemplatePart(string $template, array $context = [], string $layout = 'base')
{
    extract($context);
    include getTemplate($layout)->partial($template);
}


/**
 * @param string $filename
 *
 * @return string Absolute path to theme asset with $filename (relative to assets/dist directory)
 */
function getAssetPath(string $filename): string
{
    return get_theme_file_path('assets/dist/' . $filename);
}


/**
 * @param string $filename
 *
 * @return string Absolute URI to theme asset with $filename (relative to assets/dist directory)
 */
function getAssetUri(string $filename): string
{
    return get_theme_file_uri('assets/dist/' . $filename);
}


/**
 * @global \WP_Scripts $wp_scripts
 *
 * @param string $handle
 *
 * @return bool True if script with given $handle is enqueued in the moment.
 */
function isScriptEnqueued(string $handle): bool
{
    global $wp_scripts;
    return in_array($handle, $wp_scripts->queue);
}


/**
 * @param \WP_Post $post
 *
 * @return bool True if $post has MIME type image/*, false otherwise.
 */
function isImage(\WP_Post $post): bool {
    return strpos($post->post_mime_type, 'image/') === 0;
}


/**
 * @param \WP_Post $post
 *
 * @return bool True if $post has MIME type video/*, false otherwise.
 */
function isVideo(\WP_Post $post): bool {
    return strpos($post->post_mime_type, 'video/') === 0;
}
