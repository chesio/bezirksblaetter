<?php
/**
 * Theme provided settings:
 * - media parent page selection (on Settings > Media)
 * - gallery page selection (on Settings > Reading)
 * - number of recent media items to be displayed on home page (Settings > Reading)
 *
 * In addition, theme provides UI to change medium_large image size dimensions:
 * https://make.wordpress.org/core/2015/11/10/responsive-images-in-wordpress-4-4/
 *
 * @package Bezirksblätter
 */

namespace App\Admin;


// WordPress does not retain type of option values except for values that need to be serialized (like arrays).
// But luckily there's an easy way to fix it:
add_filter('option_' . B13R_GALLERY_PAGE, 'intval', 0);
add_filter('option_' . B13R_MEDIA_PARENT_PAGE, 'intval', 0);
add_filter('option_' . B13R_TAGS_PAGE, 'intval', 0);
add_filter('option_' . B13R_NO_OF_ITEMS_ON_HOMEPAGE, 'absint', 0);


/**
 * @link https://codex.wordpress.org/Settings_API
 */
add_action('admin_init', function () {
    // Register settings
    register_setting('media', B13R_MEDIA_PARENT_PAGE, 'intval');
    register_setting('media', 'medium_large_size_w', 'intval');
    register_setting('media', 'medium_large_size_h', 'intval');
    register_setting('reading', B13R_GALLERY_PAGE, 'intval');
    register_setting('reading', B13R_TAGS_PAGE, 'intval');
    register_setting('reading', B13R_NO_OF_ITEMS_ON_HOMEPAGE, 'absint');

    // Register setting sections
    add_settings_section(
        'bezirksblaetter-media',
        __('Bezirksblätter', 'bezirksblaetter'),
        null,
        'media'
    );
    add_settings_section(
        'bezirksblaetter-reading',
        __('Bezirksblätter', 'bezirksblaetter'),
        null,
        'reading'
    );

    // Register setting fields
    add_settings_field(
        B13R_MEDIA_PARENT_PAGE,
        __('Media parent page', 'bezirksblaetter'),
        function () { \App\Admin\render_page_selection_field(B13R_MEDIA_PARENT_PAGE); },
        'media',
        'bezirksblaetter-media',
        ['label_for' => B13R_MEDIA_PARENT_PAGE]
    );
    add_settings_field(
        'medium_large_size_w',
        __('Medium-large width', 'bezirksblaetter'),
        function () { \App\Admin\render_size_selection_field('medium_large_size_w', __('px', 'bezirksblaetter')); },
        'media',
        'bezirksblaetter-media',
        ['label_for' => 'medium_large_size_w']
    );
    add_settings_field(
        'medium_large_size_h',
        __('Medium-large height', 'bezirksblaetter'),
        function () { \App\Admin\render_size_selection_field('medium_large_size_h', __('px', 'bezirksblaetter')); },
        'media',
        'bezirksblaetter-media',
        ['label_for' => 'medium_large_size_h']
    );
    add_settings_field(
        B13R_GALLERY_PAGE,
        __('Gallery root page', 'bezirksblaetter'),
        function () { \App\Admin\render_page_selection_field(B13R_GALLERY_PAGE); },
        'reading',
        'bezirksblaetter-reading',
        ['label_for' => B13R_GALLERY_PAGE]
    );
    add_settings_field(
        B13R_TAGS_PAGE,
        __('Tags listing page', 'bezirksblaetter'),
        function () { \App\Admin\render_page_selection_field(B13R_TAGS_PAGE); },
        'reading',
        'bezirksblaetter-reading',
        ['label_for' => B13R_TAGS_PAGE]
    );
    add_settings_field(
        B13R_NO_OF_ITEMS_ON_HOMEPAGE,
        __('Homepage shows at most', 'bezirksblaetter'),
        function () { \App\Admin\render_size_selection_field(B13R_NO_OF_ITEMS_ON_HOMEPAGE, __('media items', 'bezirksblaetter')); },
        'reading',
        'bezirksblaetter-reading',
        ['label_for' => B13R_NO_OF_ITEMS_ON_HOMEPAGE]
    );
}, 10, 0);


/**
 * Render select box for $option_name setting.
 *
 * @param string $option_name
 */
function render_page_selection_field(string $option_name)
{
    // Get option value
    $option = get_option($option_name);
    //
    $pages = get_pages();

    if (empty($pages)) {
        echo '<em>' . __('There are no pages to select from.', 'bezirksblaetter') . '</em>';
    } else {
        // Render select box
        echo '<select id="' . esc_attr($option_name) . '" name="' . esc_attr($option_name) . '">';
        echo '<option value="0"></option>';
        foreach ($pages as $page) {
            echo '<option value="' . $page->ID . '"' . selected($option, $page->ID, false) . '>' . esc_html(get_the_title($page)) . '</option>';
        }
        echo '</select>';
    }
}


/**
 * Render numerical input for $option_name setting with optional additional $info.
 *
 * @param string $option_name
 * @param string $info
 */
function render_size_selection_field(string $option_name, string $info = '')
{
    // Get option value
    $option = get_option($option_name);
    // Render number input field
    echo '<input type="number" id="' . esc_attr($option_name) . '" name="' . esc_attr($option_name) . '" value="' . esc_attr($option) . '" min="0" step="1" class="small-text">';
    //
    if ($info) {
        echo ' ' . $info;
    }
}
