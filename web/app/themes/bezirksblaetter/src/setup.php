<?php
/**
 * @package Bezirksblätter
 */

use CeP\Bezirksblaetter\Template;
use CeP\Bezirksblaetter\Template\Wrapper;


/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://github.com/roots/soil
     */
    add_theme_support('soil', ['clean-up', 'nav-walker', 'nice-search']);

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'bezirksblaetter')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form', 'script', 'style']);

    /**
     * Disable block-based widget editor
     * @link https://make.wordpress.org/core/2021/06/29/block-based-widgets-editor-in-wordpress-5-8/
     */
    remove_theme_support('widgets-block-editor');

    /**
     * Use custom stylesheet for visual editor
     * @see assets/src/sass/tinymce.scss
     */
    add_editor_style('assets/dist/css/tinymce' . (SCRIPT_DEBUG ? '' : '.min') . '.css');

    /**
     * Load theme text domain
     */
    load_theme_textdomain('bezirksblaetter', get_template_directory() . '/languages');

    /**
     * Remove duotone SVGs and global styles forced on us by WordPress 5.9.
     *
     * @link https://core.trac.wordpress.org/ticket/54941
     * @link https://github.com/WordPress/gutenberg/issues/38299
     */
    remove_action('wp_body_open', 'wp_global_styles_render_svg_filters');
    remove_action('wp_enqueue_scripts', 'wp_enqueue_global_styles');
    remove_action('wp_footer', 'wp_enqueue_global_styles', 1);
}, 10, 0);


/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    // If SCRIPT_DEBUG is on, load dev versions (unminified) of assets.
    $suffix = SCRIPT_DEBUG ? '' : '.min';

    // Colcade -- only register (enqueued when needed)
    wp_register_script('colcade', \App\getAssetUri("vendor/colcade/colcade{$suffix}.js"), [], '0.2.0', true);
    // Photoswipe -- only register (enqueued when needed) scripts (styles are compiled into main.css):
    wp_register_script('photoswipe-core', \App\getAssetUri("vendor/photoswipe/photoswipe{$suffix}.js"), [], '4.1.3', true);
    wp_register_script('photoswipe-ui', \App\getAssetUri("vendor/photoswipe/photoswipe-ui-default{$suffix}.js"), ['photoswipe-core'], '4.1.3', true);
    wp_register_script('photoswipe', \App\getAssetUri("js/photoswipe{$suffix}.js"), ['photoswipe-core', 'photoswipe-ui'], filemtime(\App\getAssetPath("js/photoswipe{$suffix}.js")), true);
    // Media grid -- only register (see above)
    wp_register_script('media-grid', \App\getAssetUri("js/media-grid{$suffix}.js"), ['colcade'], filemtime(\App\getAssetPath("js/media-grid{$suffix}.js")), true);

    // Main stylesheet and script is always needed
    wp_enqueue_style('bezirksblaetter', \App\getAssetUri("css/main{$suffix}.css"), [], filemtime(\App\getAssetPath("css/main{$suffix}.css")));
    wp_enqueue_script('bezirksblaetter', \App\getAssetUri("js/main{$suffix}.js"), [], filemtime(\App\getAssetPath("js/main{$suffix}.js")), true);
}, 100, 0);


/**
 * Add Open Graph meta tags to document's <head>.
 *
 * @global \WP_Query $wp_query
 */
add_action('wp_head', function () {
    global $wp_query;
?>
    <meta property="og:site_name" content="<?= esc_attr(get_bloginfo('name')); ?>">
    <meta property="og:type" content="blog">
    <meta property="og:title" content="<?= wp_get_document_title(); ?>">
    <meta property="og:url" content="<?= esc_attr(home_url(add_query_arg(null, null))); ?>">
    <?php if (is_attachment() && \App\isImage($wp_query->get_queried_object())) { ?>
    <meta property="og:image" content="<?= esc_attr(wp_get_attachment_image_url(null, 'medium')); ?>">
    <?php }
}, 20, 0);


/**
 * Add inline JS code to document's <head> in order to:
 * - turn html.no-js into html.js
 * - make URL to icons.svg available in JS
 * - trigger actions (lights off, zoom in) according to stored state
 */
add_action('wp_head', function () {
    ?>
    <script>
        document.documentElement.classList.remove('no-js'); document.documentElement.classList.add('js');
        window.b13r = {
            content_with_svg_icon: function (content, icon_before, icon_after) {
                var output = '';
                if (icon_before) {
                    output += '<svg class="icon"><use xlink:href="<?= \App\getAssetUri('icons.svg'); ?>#' + icon_before + '"></svg>';
                }
                output += '<span class="label">' + content + '</span>';
                if (icon_after) {
                    output += '<svg class="icon"><use xlink:href="<?= \App\getAssetUri('icons.svg'); ?>#' + icon_after + '"></svg>';
                }
                return output;
            }
        };
        (function() {
            try {
                if (window['localStorage'].getItem('lights') === 'off') {
                    document.documentElement.classList.add('lights-off');
                }
                if (window['sessionStorage'].getItem('zoom') === 'out') {
                    document.documentElement.classList.add('zoomed-out');
                }
            }
            catch(e) {
                document.documentElement.classList.add('no-web-storage');
            }
        })();
    </script>
<?php
}, 100, 0);


/**
 * Use theme wrapper
 */
add_filter('template_include', function ($main) {
    if (!is_string($main) && !(is_object($main) && method_exists($main, '__toString'))) {
        return $main;
    }
    return ((new Template(new Wrapper($main)))->layout());
}, 109, 1);


/**
 * Exclude directories with no template files from WP_theme::scandir()
 *
 * @link https://core.trac.wordpress.org/ticket/38292
 *
 * @param array $exclusions
 *
 * @return array
 */
add_filter('theme_scandir_exclusions', function (array $exclusions): array {
    return array_merge($exclusions, ['assets', 'src']);
}, 10, 1);


/**
 * Alter template inclusion:
 * - Return "templates/partials/gallery.php" as page template if current page is "the gallery page".
 * - Return "templates/partials/tags.php" as page template if current page is "the tag page".
 *
 * @link https://markjaquith.wordpress.com/2014/02/19/template_redirect-is-not-for-loading-templates/
 *
 * @param string $template
 *
 * @return string
 */
add_filter('template_include', function (string $template): string {
    if (is_page(get_option(B13R_GALLERY_PAGE, 0))) {
        return locate_template('/templates/partials/galleries.php');
    }
    if (is_page(get_option(B13R_TAGS_PAGE, 0))) {
        return locate_template('/templates/partials/tags.php');
    }
    return $template;
}, 10, 1);


/**
 * Perform redirects etc.
 *
 * a) Redirect to a random media page, if current page is media parent page.
 * b) Terminate execution with 204 No Content, if attachment page is requested for media that is not assigned to media parent page.
 */
add_action('template_redirect', function () {
    $media_parent_page = get_option(B13R_MEDIA_PARENT_PAGE, 0);

    if (is_page($media_parent_page)) {
        // Get an array of one random post from page attachments
        $media = get_posts([
            'post_type' => 'attachment',
            'post_parent' => $media_parent_page,
            'numberposts' => 1,
            'orderby' => 'rand',
        ]);

        if ((count($media) > 0) && wp_redirect(get_attachment_link($media[0]), 302)) {
            exit;
        }
    } elseif (is_attachment() && (!$media_parent_page || get_queried_object()->post_parent !== $media_parent_page)) {
        // Attachment page that is not assigned to media parent page -> no content to display.
        // Note that document view does not change when a link pointing to 204 is
        // clicked, what I guess is completely fine (care should be taken to not
        // have any such links anywhere).
        wp_die('<p>' . esc_html("There is no content for this URL.", 'bezirksblaetter') . '</p>', __('No Content', 'bezirksblaetter'), ['response' => 204, 'back_link' => false]);
    }
}, 10, 0);


/**
 * Assign a random attachment to global $post when there is nothing to show (~ 404 page is about to be displayed).
 * This way we have some random data to display (header image) on 404 page.
 *
 * @global \WP_Query $wp_query
 * @global \WP_Post $post
 */
add_action('wp', function () {
    /** @var \WP_Query $wp_query */
    global $wp_query;

    /** @var \WP_Post $post */
    global $post;

    if ($wp_query->is_404() && !empty($media_parent_page = get_option(B13R_MEDIA_PARENT_PAGE, 0))) {
        // Get an array of one random post from page attachments
        $media = get_posts([
            'post_type' => 'attachment',
            'post_parent' => $media_parent_page,
            'numberposts' => 1,
            'orderby' => 'rand',
        ]);

        if (count($media) === 1) {
            $post = array_shift($media);
        }
    }
});
