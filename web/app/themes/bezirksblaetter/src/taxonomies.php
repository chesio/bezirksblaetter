<?php
/**
 * Register two taxonomies for media (attachment) post type:
 * - galleries (hierarchical)
 * - tags (flat)
 * @link https://generatewp.com/taxonomy/
 *
 * Add some enhancements for both taxonomies:
 * - make term group field editable via backend to manage galleries order
 * - new RTE description field as term meta (for both galleries and tags)
 * @link https://www.smashingmagazine.com/2015/12/how-to-use-term-meta-data-in-wordpress/
 *
 * Both taxonomies have _update_generic_term_count set as update_count_callback:
 * @link https://codex.wordpress.org/Function_Reference/register_taxonomy#Arguments
 *
 * @package Bezirksblätter
 * @todo Move to a plugin.
 */

namespace App\Taxonomies;


define('MEDIA_GALLERIES_TAXONOMY', 'media_galleries');
define('MEDIA_TAGS_TAXONOMY', 'media_tags');


/**
 * Media galleries are like post categories (hierarchical)
 */
function register_media_galleries()
{
    // Run only once
    if (taxonomy_exists(MEDIA_GALLERIES_TAXONOMY)) {
        return;
    }

    $labels = [
        'name'                       => _x('Galleries', 'Taxonomy General Name', 'bezirksblaetter'),
        'singular_name'              => _x('Gallery', 'Taxonomy Singular Name', 'bezirksblaetter'),
        'menu_name'                  => __('Galleries', 'bezirksblaetter'),
        'all_items'                  => __('All Galleries', 'bezirksblaetter'),
        'parent_item'                => __('Parent Gallery', 'bezirksblaetter'),
        'parent_item_colon'          => __('Parent Gallery:', 'bezirksblaetter'),
        'new_item_name'              => __('New Gallery Name', 'bezirksblaetter'),
        'add_new_item'               => __('Add Gallery', 'bezirksblaetter'),
        'edit_item'                  => __('Edit Gallery', 'bezirksblaetter'),
        'update_item'                => __('Update Gallery', 'bezirksblaetter'),
        'view_item'                  => __('View Gallery', 'bezirksblaetter'),
        'separate_items_with_commas' => __('Separate galleries with commas', 'bezirksblaetter'),
        'add_or_remove_items'        => __('Add or remove galleries', 'bezirksblaetter'),
        'choose_from_most_used'      => __('Choose from the most used galleries', 'bezirksblaetter'),
        'popular_items'              => __('Popular Galleries', 'bezirksblaetter'),
        'search_items'               => __('Search Galleries', 'bezirksblaetter'),
        'not_found'                  => __('Not Found', 'bezirksblaetter'),
        'no_terms'                   => __('No galleries', 'bezirksblaetter'),
    ];
    $rewrite = [
        'slug'                      => 'media-galleries',
        'with_front'                => true,
        'hierarchical'              => true,
    ];
    $args = [
        'labels'                    => $labels,
        'hierarchical'              => true,
        'public'                    => true,
        'show_ui'                   => true,
        'show_admin_column'         => true,
        'show_in_nav_menus'         => true,
        'show_tagcloud'             => false,
        'rewrite'                   => $rewrite,
        'update_count_callback'     => '_update_generic_term_count',
    ];

    register_taxonomy(MEDIA_GALLERIES_TAXONOMY, [ 'attachment' ], $args);
}
// Hook into the 'init' action
add_action('init', __NAMESPACE__ . '\\register_media_galleries', 0);


/**
 * Media tags are like post tags (non-hierarchical)
 */
function register_media_tags()
{
    // Run only once
    if (taxonomy_exists(MEDIA_TAGS_TAXONOMY)) {
        return;
    }

    $labels = [
        'name'                       => _x('Tags', 'Taxonomy General Name', 'bezirksblaetter'),
        'singular_name'              => _x('Tag', 'Taxonomy Singular Name', 'bezirksblaetter'),
        'menu_name'                  => __('Tags', 'bezirksblaetter'),
        'all_items'                  => __('All Tags', 'bezirksblaetter'),
        'parent_item'                => __('Parent Tag', 'bezirksblaetter'),
        'parent_item_colon'          => __('Parent Tag:', 'bezirksblaetter'),
        'new_item_name'              => __('New Tag Name', 'bezirksblaetter'),
        'add_new_item'               => __('Add Tag', 'bezirksblaetter'),
        'edit_item'                  => __('Edit Tag', 'bezirksblaetter'),
        'update_item'                => __('Update Tag', 'bezirksblaetter'),
        'view_item'                  => __('View Tag', 'bezirksblaetter'),
        'separate_items_with_commas' => __('Separate tags with commas', 'bezirksblaetter'),
        'add_or_remove_items'        => __('Add or remove tags', 'bezirksblaetter'),
        'choose_from_most_used'      => __('Choose from the most used tags', 'bezirksblaetter'),
        'popular_items'              => __('Popular Tags', 'bezirksblaetter'),
        'search_items'               => __('Search Tags', 'bezirksblaetter'),
        'not_found'                  => __('Not Found', 'bezirksblaetter'),
        'no_terms'                   => __('No tags', 'bezirksblaetter'),
    ];
    $rewrite = [
        'slug'                      => 'media-tags',
        'with_front'                => true,
        'hierarchical'              => false,
    ];
    $args = [
        'labels'                    => $labels,
        'hierarchical'              => false,
        'public'                    => true,
        'show_ui'                   => true,
        'show_admin_column'         => true,
        'show_in_nav_menus'         => true,
        'show_tagcloud'             => true,
        'rewrite'                   => $rewrite,
        'update_count_callback'     => '_update_generic_term_count',
    ];

    register_taxonomy(MEDIA_TAGS_TAXONOMY, [ 'attachment' ], $args);
}
// Hook into the 'init' action
add_action('init', __NAMESPACE__ . '\\register_media_tags', 0);


/**
 * Add term-group field to "Add term" screen.
 */
function add_term_group_field()
{
?>
    <div class="form-field term-group">
        <label for="term_group"><?= esc_html__('Order', 'bezirksblaetter'); ?></label>
        <input type="text" id="term_group" name="term_group">
    </div>
<?php
}
add_action(MEDIA_GALLERIES_TAXONOMY . '_add_form_fields', __NAMESPACE__ . '\\add_term_group_field', 10, 0);


/**
 * Add `term-group` field to "Edit term" screen.
 * @link https://developer.wordpress.org/reference/classes/_wp_editors/parse_settings/
 * @param \WP_Term $term
 */
function edit_term_group_field(\WP_Term $term)
{
?>
    <tr class="form-field term-group-wrap">
        <th scope="row"><label for="term_group"><?= esc_html__('Order', 'bezirksblaetter'); ?></label></th>
        <td><input name="term_group" id="term_group" value="<?= $term->term_group; ?>" size="20" aria-required="true" type="text"></td>
    </tr>
<?php
}
add_action(MEDIA_GALLERIES_TAXONOMY . '_edit_form_fields', __NAMESPACE__ . '\\edit_term_group_field', 10, 1);


/**
 * Add `story` field to "Edit term" screen.
 * @link https://developer.wordpress.org/reference/classes/_wp_editors/parse_settings/
 * @param \WP_Term $term
 */
function edit_term_story_field(\WP_Term $term)
{
    // Get the story (long description)
    $story = get_term_meta($term->term_id, 'story', true);
?>
    <tr class="form-field term-story-wrap">
        <th scope="row"><label for="term_story"><?= esc_html__('Story', 'bezirksblaetter'); ?></label></th>
        <td><?php wp_editor($story, 'term_story', ['media_buttons' => false, 'teeny' => true]); ?></td>
    </tr>
<?php
}
add_action(MEDIA_GALLERIES_TAXONOMY . '_edit_form_fields', __NAMESPACE__ . '\\edit_term_story_field', 10, 1);
add_action(MEDIA_TAGS_TAXONOMY . '_edit_form_fields', __NAMESPACE__ . '\\edit_term_story_field', 10, 1);


/**
 * Term group is not saved automatically on creation, thus this hook.
 * @param array $data
 * @param string $taxonomy
 * @return array
 */
function wp_insert_term_data(array $data, string $taxonomy): array
{
    if ($taxonomy === MEDIA_GALLERIES_TAXONOMY && !$data['term_group']) {
        // Only manipulate term group of media gallery taxonomy and only if it was not set by WordPress already (this
        // can happen if term is an alias for another term).
        $term_group = filter_input(INPUT_POST, 'term_group', FILTER_VALIDATE_INT);
        if (is_int($term_group)) {
            $data['term_group'] = $term_group;
        }
    }

    return $data;
}
add_filter('wp_insert_term_data', __NAMESPACE__ . '\\wp_insert_term_data', 10, 2);


/**
 * Save term meta data (now only the `story`, cause `term_group` is saved by `update_term_meta`).
 * @param int $term_id
 */
function edited(int $term_id)
{
    $story = wp_kses_post(filter_input(INPUT_POST, 'term_story'));
    update_term_meta($term_id, 'story', $story);
}
add_action('edited_' . MEDIA_GALLERIES_TAXONOMY, __NAMESPACE__ . '\\edited');
add_action('edited_' . MEDIA_TAGS_TAXONOMY, __NAMESPACE__ . '\\edited');
