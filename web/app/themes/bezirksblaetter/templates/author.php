<?php
/**
 * Displays author bio (do not display author posts).
 *
 * @package Bezirksblätter
 * @version 20170701
 */

get_template_part('partials/page-header');
?>
<section>
    <?= wpautop(get_the_author_meta('description')); ?>
</section>
