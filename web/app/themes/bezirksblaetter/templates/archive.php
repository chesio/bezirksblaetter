<?php
/**
 * Archive page displays:
 * - blog category pages (category.php)
 * - blog tag pages (tag.php)
 * - date-based archive pages (date.php)
 *
 * @package Bezirksblätter
 * @version 20200201
 */

/** @var \WP_Term */
$term = $wp_query->get_queried_object();

get_template_part('partials/nav-breadcrumb');
get_template_part('partials/page-header');

if (!empty($term->description)) {
    // Section: term description
?>
    <section>
    <?php \App\includeTemplatePart('partials/term-description', ['term' => $term]); ?>
    </section>
<?php
}

if (!have_posts()) {
?>
    <section>
        <p class="message"><?= esc_html__('Sad panda! No results were found.', 'bezirksblaetter'); ?></p>
        <?php get_search_form(); ?>
    </section>
<?php
}

while (have_posts()) {
    the_post();
    //
    get_template_part('partials/content', is_search() ? 'search' : (get_post_type() === 'post' ? get_post_format() : get_post_type()));
}

// Display posts navigation
the_posts_navigation();
