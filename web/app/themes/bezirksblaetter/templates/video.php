<?php
/**
 * Template for video attachments page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bezirksblätter
 * @version 20200201
 */

// If instead of usual while - we're assuming only single post here.
if (have_posts()) {
    //
    the_post();

    $post_id = $post->ID;

    // Video caption and title
    $video_caption = wp_get_attachment_caption($post_id);
    $video_title = get_the_title($post);
    $video_thumbnail = get_the_post_thumbnail_url($post_id, 'full');
    $video_metadata = wp_get_attachment_metadata($post_id);
    $video_width = $video_metadata['width'];
    $video_height = $video_metadata['height'];
?>
    <div class="image-header" id="image-header">
        <figure itemscope itemtype="http://schema.org/VidoeObject">
            <div class="responsive-embed <?= \App\getVideoRatio($video_width, $video_height); ?>">
                <video id="fullscreen-element" src="<?= wp_get_attachment_url($post_id); ?>" controls poster="<?= $video_thumbnail ?: ''; ?>"></video>
            </div>
            <?php if ($video_caption) { ?>
            <figcaption itemprop="caption description"><?= esc_html($video_caption); ?></figcaption>
            <?php } ?>
            <meta itemprop="name" content="<?= esc_attr($video_title); ?>">
            <meta itemprop="contentUrl" content="<?= wp_get_attachment_url($post_id); ?>">
            <meta itemprop="width" content="<?= $video_width; ?>">
            <meta itemprop="height" content="<?= $video_height; ?>">
        </figure>
        <header>
            <h1><?= esc_html($video_title) ?></h1>
        </header>
        <nav class="adjacent">
            <div class="container">
                <?php \previous_media_link(\App\svgUseIcon('chevron-left')); ?>
                <?php \next_media_link(\App\svgUseIcon('chevron-right')); ?>
            </div>
        </nav>
        <nav class="actions">
            <div class="container">
                <button id="fullscreen-toggler" class="fullscreen-toggler"><?= \App\svgUseIcon('arrow-maximise'); ?></button>
            </div>
        </nav>
    </div>

    <?php if ($post->post_content || $video_caption) { ?>
    <section>
        <?php
        // Prefer description (post content) over caption.
        if ($post->post_content) {
            echo '<div class="the-content">';
            the_content(); // ~ wraps content in <p> tags automatically
            echo '</div>';
        } elseif ($video_caption) {
            echo '<p>' . esc_html($video_caption) . '</p>'; // ~ manual wrapping
        }
        ?>
    </section>
    <?php } ?>

    <section>
        <?php get_template_part('partials/media-meta'); ?>
    </section>

<?php

// Allow videos to have comments
comments_template('/templates/partials/comments.php');

} // if (have_posts())
