<?php
/**
 * The template for displaying a list of all galleries.
 *
 * Any page set as (root) gallery page is rendered via this template.
 *
 * @package Bezirksblätter
 * @version 20190204
 */

//
get_template_part('partials/page-header');
?>
<section>
<?php

// Print any page content first:
while (have_posts()) {
    the_post();
    ?>
    <div class="content the-content">
        <?php the_content(); ?>
    </div>
<?php
}

// Print list of all galleries then:
$galleries = get_terms(['taxonomy' => MEDIA_GALLERIES_TAXONOMY]);

if (count($galleries) > 0) { ?>
    <nav class="galleries content">
        <ul class="fancy">
        <?php
            wp_list_categories([
                'taxonomy' => MEDIA_GALLERIES_TAXONOMY,
                'title_li' => '',
                'show_count' => true,
                'orderby' => 'term_group',
                'order' => 'DESC',
                'use_desc_for_title' => false,
                'walker' => new \CeP\Bezirksblaetter\WalkerCategory($galleries, MEDIA_GALLERIES_TAXONOMY),
            ]);
        ?>
        </ul>
    </nav>
<?php } else { ?>
    <p class="message"><?= esc_html__('Sad panda! There are no published galleries yet.', 'bezirksblaetter'); ?></p>
<?php } ?>
</section>
