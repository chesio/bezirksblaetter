<?php
/**
 * The template for displaying a list of all tags.
 *
 * Any page set as tag (listing) page is rendered via this template.
 *
 * @package Bezirksblätter
 * @version 20190204
 */

//
get_template_part('partials/page-header');
?>
<section>
<?php

// Print any page content first:
while (have_posts()) {
    the_post();
?>
    <div class="content the-content">
        <?php the_content(); ?>
    </div>
<?php
}

// Print list of all tags then:
$tags = get_terms(['taxonomy' => MEDIA_TAGS_TAXONOMY]);

if (count($tags) > 0) { ?>
    <nav class="tags content">
        <ul class="fancy">
        <?php
            wp_list_categories([
                'taxonomy' => MEDIA_TAGS_TAXONOMY,
                'title_li' => '',
                'show_count' => true,
                'orderby' => 'name',
                'use_desc_for_title' => false,
                'walker' => new \CeP\Bezirksblaetter\WalkerCategory($tags, MEDIA_TAGS_TAXONOMY),
            ]);
        ?>
        </ul>
    </nav>
<?php } else { ?>
    <p class="message"><?= esc_html__('Sad panda! There are no published tags yet.', 'bezirksblaetter'); ?></p>
<?php } ?>
</section>
