<?php
/**
 * PhotoSwipe viewer template.
 *
 * @link http://photoswipe.com/documentation/getting-started.html
 *
 * @package Bezirksblätter
 * @version 20170701
 */
?>
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="<?= esc_attr_x('Close (Esc)', 'PhotoSwipe UI', 'bezirksblaetter'); ?>"></button>
                <button class="pswp__button pswp__button--share" title="<?= esc_attr_x('Share', 'PhotoSwipe UI', 'bezirksblaetter'); ?>"></button>
                <button class="pswp__button pswp__button--fs" title="<?= esc_attr_x('Toggle fullscreen', 'PhotoSwipe UI', 'bezirksblaetter'); ?>"></button>
                <button class="pswp__button pswp__button--zoom" title="<?= esc_attr_x('Zoom in/out', 'PhotoSwipe UI', 'bezirksblaetter'); ?>"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="<?= esc_attr_x('Previous (arrow left)', 'PhotoSwipe UI', 'bezirksblaetter'); ?>"></button>
            <button class="pswp__button pswp__button--arrow--right" title="<?= esc_attr_x('Next (arrow right)', 'PhotoSwipe UI', 'bezirksblaetter'); ?>"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
