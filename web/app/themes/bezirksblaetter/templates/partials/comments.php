<?php
/**
 * Comments template: comments + form.
 *
 * @package Bezirksblätter
 * @version 20200201
 */

if (post_password_required()) {
    return;
}
?>

<section id="comments" class="comments">
    <?php if (have_comments()) { ?>
    <h2><?php printf(_nx('One response to &ldquo;%2$s&rdquo;', '%1$s responses to &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'bezirksblaetter'), number_format_i18n(get_comments_number()), '<span>' . get_the_title() . '</span>'); ?></h2>

    <ol class="comment-list">
        <?php wp_list_comments(['style' => 'ol', 'short_ping' => true]); ?>
    </ol>

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) { ?>
        <nav class="comments-pager">
            <ul>
                <?php if (get_previous_comments_link()) { ?>
                <li class="previous"><?= get_previous_comments_link(sprintf('%s<span class="label">%s</span>', \App\svgUseIcon('long-arrow-left'), __('Older comments', 'bezirksblaetter'))); ?></li>
                <?php } ?>
                <?php if (get_next_comments_link()) { ?>
                <li class="next"><?= get_next_comments_link(sprintf('<span class="label">%s</span>%s', __('Newer comments', 'bezirksblaetter'), \App\svgUseIcon('long-arrow-right'))); ?></li>
                <?php } ?>
            </ul>
        </nav>
        <?php } ?>

    <?php } // have_comments() ?>

    <?php if (!comments_open() && get_comments_number() !== 0 && post_type_supports(get_post_type(), 'comments')) { ?>
    <div class="alert alert-warning">
        <?= esc_html__('Comments are closed.', 'bezirksblaetter'); ?>
    </div>
    <?php } ?>

    <?php comment_form(); ?>
</section>
