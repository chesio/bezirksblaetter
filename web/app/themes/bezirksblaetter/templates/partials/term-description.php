<?php
/**
 * Template displays term description: RTE story meta field (if any) or
 * plain-text description.
 *
 * @package Bezirksblätter
 * @version 20170701
 */

// Note: $term must be made available in file's scope

$story = get_term_meta($term->term_id, 'story', true);
// If there's a story, display it, otherwise display the description.
if ($story || $term->description) { ?>
    <div class="content the-content">
        <?= $story ? apply_filters('the_content', $story) : esc_html($term->description); ?>
    </div>
<?php
}
