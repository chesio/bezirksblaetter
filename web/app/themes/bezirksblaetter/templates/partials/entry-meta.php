<?php
/**
 * @package Bezirksblätter
 * @version 20170701
 */
?>
<p class="entry-meta"><time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_post_time('d.m.Y H:i'); ?></time></p>
