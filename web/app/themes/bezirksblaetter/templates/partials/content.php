<?php
/**
 * @package Bezirksblätter
 * @version 20200201
 */
?>
<article <?php post_class(); ?>>
    <?php
    if (has_post_thumbnail($post)) {
        echo '<a class="thumbnail" href="' . get_permalink($post) . '">' . get_the_post_thumbnail($post, 'thumbnail') . '</a>';
    } else {
        echo '<a class="no-thumbnail" href="' . get_permalink($post) . '">' . \App\svgUseIcon('camera') . '</a>';
    }
    ?>
    <header>
        <h2 class="entry-title"><a href="<?= get_permalink(); ?>"><?= \App\contentWithSvgIcon(get_the_title(), '', 'long-arrow-right'); ?></a></h2>
        <?php get_template_part('partials/entry-meta'); ?>
    </header>
    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>
</article>
