<?php
/**
 * @package Bezirksblätter
 * @version 20200201
 */
?>
<article <?php post_class(); ?>>
    <?php
    if ($post->post_type === 'attachment' && \App\isImage($post)) {
        // Image attachment thumbnail
        echo '<a class="thumbnail" href="' . get_permalink($post) . '">' . wp_get_attachment_image($post->ID, 'thumbnail') . '</a>';
    } elseif ($post->post_type === 'post' && has_post_thumbnail($post)) {
        // Post thumbnail
        echo '<a class="thumbnail" href="' . get_permalink($post) . '">' . get_the_post_thumbnail($post, 'thumbnail') . '</a>';
    } else {
        // No thumbnail
        echo '<a class="no-thumbnail" href="' . get_permalink($post) . '">' . \App\svgUseIcon('camera') . '</a>';
    }
    ?>
    <header>
        <h2 class="entry-title"><a href="<?= get_permalink(); ?>"><?= \App\contentWithSvgIcon(get_the_title(), '', 'long-arrow-right'); ?></a></h2>
        <?php if ($post->post_type === 'post') { get_template_part('partials/entry-meta'); } ?>
    </header>
    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>
</article>
