<?php
/**
 * Template partial that renders attachment media meta data.
 *
 * @version 20200201
 */

global $post;

$post_id = $post->ID;

// Get filename and meta data of given post.
$filename = get_attached_file($post_id);
$metadata = wp_get_attachment_metadata($post_id);

// Notes:
// 1) Some meta data are not available for both image and video.
// 2) Some meta data are not stored in the same way.
$is_image = \App\isImage($post);
$is_video = \App\isVideo($post);

// Image EXIF data:
$image_meta = \App\isImage($post) ? $metadata['image_meta'] : [];

// Timestamp of creation:
$timestamp = isset($image_meta['created_timestamp']) ? intval($image_meta['created_timestamp']) : null;

// Galleries this media file is part of.
$galleries = array_map(
    function($term) {
        return '<a href="' . get_term_link($term) . '" title="' . esc_attr($term->name) . '">' . $term->name . '</a>';
    },
    wp_get_post_terms($post_id, MEDIA_GALLERIES_TAXONOMY)
);

// Tags this media file has been tagged with.
$tags = array_map(
    function($term) {
        return '<a href="' . get_term_link($term) . '" title="' . esc_attr($term->name) . '">' . $term->name . '</a>';
    },
    wp_get_post_terms($post_id, MEDIA_TAGS_TAXONOMY)
);

// Media file size:
$file_size = filesize($filename);

// Media file URL:
$url = wp_get_attachment_url($post_id, 'full');

// Video length:
$video_length = $is_video ? $metadata['length_formatted'] : null;

?>
<ul class="media-meta">
<?php if (!is_null($timestamp)) { ?>
    <li title="<?= esc_attr__('Date', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(date('d.m.Y', $timestamp), 'calendar'); ?></li>
    <li title="<?= esc_attr__('Time', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(date('H:i:s', $timestamp), 'clock-o'); ?></li>
<?php } ?>
<?php if (count($galleries) > 0) { ?>
    <li title="<?= esc_attr__('Galleries', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(implode(', ', $galleries), 'folder-open'); ?></li>
<?php } ?>
<?php if (count($tags) > 0) { ?>
    <li title="<?= esc_attr__('Tags', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(implode(', ', $tags), 'tags'); ?></li>
<?php } ?>
</ul>

<ul class="media-meta">
<?php if (!empty($file_size)) { ?>
    <li title="<?= esc_attr__('Size', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(sprintf("%.1f MB", round($file_size / MB_IN_BYTES, 1)), $is_video ? 'film' : 'image'); ?></li>
<?php } ?>
    <li title="<?= esc_attr__('Width', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(sprintf('%dpx', $metadata['width']), 'arrows-h'); ?></li>
    <li title="<?= esc_attr__('Height', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(sprintf('%dpx', $metadata['height']), 'arrows-v'); ?></li>
<?php if (!empty($url) && !empty($filename)) { ?>
    <li><a href="<?= esc_url($url); ?>" title="<?= esc_attr__('Save image on your hard drive.', 'bezirksblaetter'); ?>" download="<?= esc_attr(basename($filename)); ?>"><?= \App\svgUseIcon('floppy-o'); ?></a></li>
<?php } ?>
</ul>

<ul class="media-meta">
<?php if (isset($image_meta['camera'])) { ?>
    <li title="<?= esc_attr__('Camera model', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(esc_html($image_meta['camera']), 'camera-fa'); ?></li>
<?php } ?>
<?php if (isset($image_meta['aperture'])) { ?>
    <li title="<?= esc_attr__('Aperture', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(sprintf('f/%d', $image_meta['aperture']), 'sun-o'); ?></li>
<?php } ?>
<?php if (isset($image_meta['shutter_speed'])) { ?>
    <li title="<?= esc_attr__('Shutter speed', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(sprintf(esc_html__('%s sec', 'bezirksblaetter'), $image_meta['shutter_speed']), 'tachometer'); ?></li>
<?php } ?>
<?php if (isset($image_meta['focal_length'])) { ?>
    <li title="<?= esc_attr__('Focal length', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(sprintf(esc_html__('%s mm', 'bezirksblaetter'), $image_meta['focal_length']), 'binoculars'); ?></li>
<?php } ?>
<?php if (isset($image_meta['iso'])) { ?>
    <li title="<?= esc_attr__('ISO speed', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(sprintf(esc_html__('%s ISO'), $image_meta['iso']), 'eye'); ?></li>
<?php } ?>
<?php if (!is_null($video_length)) { ?>
    <li title="<?= esc_attr__('Video length', 'bezirksblaetter'); ?>"><?= \App\contentWithSvgIcon(human_readable_duration($video_length), 'hourglass'); ?></li>
<?php } ?>
</ul>
