<?php
/**
 * Template for site-wide primary header
 *
 * @package Bezirksblätter
 * @version 20200201
 */
?>
<header class="primary">

    <input type="checkbox" id="nav-trigger" class="nav-trigger" />
    <div class="container">
        <label for="nav-trigger" class="nav-trigger-icon">
            <?= \App\svgUseIcon('th-menu', 'open'); ?><?= \App\svgUseIcon('times-outline', 'close'); ?>
        </label>
        <button id="turn-lights-on" class="mod-switcher  mod-switcher--lights-on"><?= \App\svgUseIcon('weather-sunny'); ?></button>
        <button id="turn-lights-off" class="mod-switcher  mod-switcher--lights-off"><?= \App\svgUseIcon('weather-night'); ?></button>
    </div>

    <nav class="primary" id="primary-nav">
        <div class="container">
    <?php
        if (has_nav_menu('primary_navigation')) {
            wp_nav_menu([
                'theme_location' => 'primary_navigation',
                'menu_class' => 'pages',
                'item_spacing' => 'discard',
            ]);
        }
        if (function_exists('pll_the_languages')) { ?>
            <ul class="languages">
                <?php pll_the_languages([
                    'hide_current' => false,
                    'show_flags' => true,
                    'show_names' => true,
                ]); ?>
            </ul>
        <?php } ?>
        </div>
    </nav>

    <nav class="contextual">
        <?php $contextual_menu = \App\contextualMenu(); ?>
        <ul class="<?= $contextual_menu['class']; ?>">
        <?php
            array_walk($contextual_menu['items'], function($item) {
                echo '<li class="' . ($item['class'] ?? '') . '">';
                    echo '<a href="' . esc_url($item['href']) . '" title="' . esc_attr($item['title']) . '"' . (isset($item['rel']) ? (' rel="' . $item['rel'] . '"') : '') . '>';
                        echo $item['content'] ?? esc_html($item['name']);
                    echo '</a>';
                echo '</li>';
            });
        ?>
        </ul>
    </nav>

</header>
