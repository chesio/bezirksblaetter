<?php
/**
 * Single (blog) post template
 *
 * @package Bezirksblätter
 * @version 20220113
 */

if (has_post_thumbnail()) {
?>
<div class="image-header vmin-75" id="image-header">
    <figure>
        <?= get_the_post_thumbnail($post, 'large', ['data-skip-lazy' => true]); // Output large thumbnail and let the browser decide based on srcset ?>
    </figure>
    <header>
        <h1 class="entry-title"><?= get_the_title(); ?></h1>
    </header>
    <?php // Note: as long as image header is optional, it does not make sense to include adjacent posts navigation... */ ?>
</div>
<?php
}
?>

<article <?php post_class(); ?>>
    <?php if(!has_post_thumbnail()) { ?>
    <header>
        <h1 class="entry-title"><?= get_the_title(); ?></h1>
    </header>
    <?php } ?>
    <?php get_template_part('partials/entry-meta'); ?>
    <div class="entry-content the-content">
        <?php the_content(); ?>
    </div>
    <footer>
        <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . esc_html__('Pages:', 'bezirksblaetter'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/partials/comments.php'); ?>
</article>
