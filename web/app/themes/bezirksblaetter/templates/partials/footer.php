<?php
/**
 * Template for site-wide primary footer
 *
 * @package Bezirksblätter
 * @version 20200201
 */
?>
<hr>
<footer class="primary">
    <?php // dynamic_sidebar('sidebar-footer'); ?>
    <p><a href="<?= esc_url(B13R_REPO) ?>" class="repo"><?= get_bloginfo('title'); ?></a> = <a href="https://www.chesio.com">Chester</a> + <a href="https://www.wordpress.org">WordPress</a> + <a href="https://roots.io/bedrock/">Bedrock</a> + <a href="https://roots.io/sage/">Sage</a> + <a href="https://polylang.pro">Polylang</a> + <a href="https://get.foundation/">Foundation</a> + <a href="https://github.com/desandro/colcade">Colcade</a> + <a href="https://photoswipe.com/">PhotoSwipe</a> + <a href="https://www.opensans.com/">Open Sans</a> + <a href="http://www.typicons.com/">Typicons</a> + <a href="https://fontawesome.com/">Font Awesome</a> + <a href="https://github.com/malchata/yall.js">yall.js</a> + <a href="http://samherbert.net/svg-loaders/">SVG loaders</a></p>
    <p>License? <a href="https://unsplash.com/license">Do whatever you want.</a> Btw. on <a href="https://unsplash.com/">Unsplash</a> you will find much better photos...</p>
</footer>
