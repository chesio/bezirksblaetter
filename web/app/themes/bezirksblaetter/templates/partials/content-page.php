<?php
/**
 * @package Bezirksblätter
 * @version 20170701
 */
?>
<section>
    <div class="content the-content">
        <?php the_content(); ?>
    </div>
</section>
