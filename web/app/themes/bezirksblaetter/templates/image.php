<?php
/**
 * Template for image attachments page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bezirksblätter
 * @version 20220113
 */

// If instead of usual while - we're assuming only single post here.
if (have_posts()) {
    //
    the_post();

    $post_id = $post->ID;

    // Image caption and title
    $image_caption = wp_get_attachment_caption($post_id);
    $image_title = get_the_title($post);

    // Full image attributtes
    $image_attrs_full = wp_get_attachment_image_src($post_id, 'full');
?>
    <div class="image-header vh-100" id="image-header">
        <figure itemscope itemtype="http://schema.org/ImageObject">
            <?= wp_get_attachment_image($post_id, 'large', false, ['data-skip-lazy' => true]); // Output large thumbnail and let the browser decide based on srcset ?>
            <?php if ($image_caption) { ?>
            <figcaption itemprop="caption description"><?= esc_html($image_caption); ?></figcaption>
            <?php } ?>
            <meta itemprop="name" content="<?= esc_attr($image_title); ?>">
            <meta itemprop="contentUrl" content="<?= wp_get_attachment_url($attachment_id); ?>">
            <meta itemprop="thumbnailUrl" content="<?= wp_get_attachment_image_url($attachment_id, 'medium'); ?>">
            <meta itemprop="width" content="<?= $image_attrs_full[1]; ?>">
            <meta itemprop="height" content="<?= $image_attrs_full[2]; ?>">
        </figure>
        <header>
            <h1><?= esc_html($image_title) ?></h1>
        </header>
        <nav class="adjacent">
            <div class="container">
                <?php \previous_media_link(\App\svgUseIcon('chevron-left')); ?>
                <?php \next_media_link(\App\svgUseIcon('chevron-right')); ?>
            </div>
        </nav>
        <nav class="actions">
            <div class="container">
                <button id="zoom-in" class="zoom-in"><?= \App\svgUseIcon('zoom-in-outline'); ?></button>
                <button id="zoom-out" class="zoom-out"><?= \App\svgUseIcon('zoom-out-outline'); ?></button>
            </div>
        </nav>
    </div>

    <?php if ($post->post_content || $image_caption) { ?>
    <section>
        <?php
        // Prefer description (post content) over caption in this place. Caption is always shown in PhotoSwipe.
        if ($post->post_content) {
            echo '<div class="the-content">';
            the_content(); // ~ wraps content in <p> tags automatically
            echo '</div>';
        } elseif ($image_caption) {
            echo '<p>' . esc_html($image_caption) . '</p>'; // ~ manual wrapping
        }
        ?>
    </section>
    <?php } ?>

    <section>
        <?php get_template_part('partials/media-meta'); ?>
    </section>
<?php

// Allow photos to have comments
comments_template('/templates/partials/comments.php');

} // if (have_posts())
