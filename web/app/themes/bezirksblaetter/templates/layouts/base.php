<?php
/**
 * Base template - uses theme wrapping functionality from Sage to avoid CRY
 * when dealing with default WordPress templating engine.
 *
 * @link https://roots.io/sage/docs/theme-wrapper/
 *
 * @package Bezirksblätter
 * @version 20200201
 */
?>
<!doctype html>
<html <?= get_language_attributes(); ?> class="no-js">
    <?php get_template_part('partials/head'); // calls wp_head() ?>
    <body <?php body_class(); ?>>
    <?php
        wp_body_open();
        do_action('get_header');
        get_template_part('partials/header');
    ?>
    <main>
        <?php include \App\getTemplate()->main(); ?>
    </main>
    <?php
        do_action('get_footer');
        get_template_part('partials/footer');
        if (\App\isScriptEnqueued('photoswipe') ) {
            get_template_part('partials/photoswipe');
        }
        wp_footer();
    ?>
  </body>
</html>
