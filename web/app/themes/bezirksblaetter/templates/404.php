<?php
/**
 * 404 error page. The very home of sad panda.
 *
 * @package Bezirksblätter
 * @version 20220113
 */

if (isset($post) && ($post instanceof \WP_Post)) {
?>
    <div class="image-header vmin-65" id="image-header">
        <figure>
            <?= wp_get_attachment_image($post->ID, 'large'); // Output large thumbnail and let the browser decide based on srcset ?>
        </figure>
        <?php get_template_part('partials/page-header'); ?>
    </div>
<?php
}
?>
<section>
    <p>
        <?= esc_html__('Sad panda! The page you were trying to view does not exist.', 'bezirksblaetter'); ?>
    </p>
    <p>
        <?= esc_html__("If you know what you're looking for, go ahead and search for it:", 'bezirksblaetter'); ?>
    </p>
    <?= get_search_form(); ?>
    <p>
        <?= esc_html__("If you don't know what you're looking for, why not try one of the following options?", 'bezirksblaetter'); ?>
    </p>
    <p>
        <a class="button secondary" href="<?= home_url(); ?>"><?= esc_html__('Go home', 'bezirksblaetter'); ?></a>
        <?php if (($gallery_page = get_permalink(get_option(B13R_GALLERY_PAGE, 0)))) { ?>
        <a class="button secondary" href="<?= esc_url($gallery_page); ?>"><?= esc_html__('Browse media albums', 'bezirksblaetter'); ?></a>
        <?php } ?>
        <?php if (($posts_page = get_permalink(get_option('page_for_posts', 0)))) { ?>
        <a class="button secondary" href="<?= esc_url($posts_page); ?>"><?= esc_html__('Read my blog', 'bezirksblaetter'); ?></a>
        <?php } ?>
        <?php if (($media_parent_page = get_permalink(get_option(B13R_MEDIA_PARENT_PAGE, 0)))) { ?>
        <a class="button secondary" href="<?= esc_url($media_parent_page); ?>"><?= esc_html__('See random photo or video', 'bezirksblaetter'); ?></a>
        <?php } ?>
    </p>
</section>