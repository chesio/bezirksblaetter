<?php
/**
 * Front page displays page content followed by image grid with recent media.
 *
 * @package Bezirksblätter
 * @version 20170701
 */

// Display page content first
while (have_posts()) {
    the_post();

    get_template_part('partials/page-header');
    get_template_part('partials/content-page');
}

// Fetch media
$media_query = new \WP_Query([
    'post_type' => 'attachment',
    'post_parent' => get_option(B13R_MEDIA_PARENT_PAGE, 0),
    'post_status' => 'inherit', // necessary in case of attachments (!)
    'posts_per_page' => get_option(B13R_NO_OF_ITEMS_ON_HOMEPAGE, 16),
    'paged' => get_query_var('paged', 1),
]);

if ($media_query->have_posts()) {
    // Enqueue PhotoSwipe and media grid scripts
    wp_enqueue_script('photoswipe');
    wp_enqueue_script('media-grid');

    // Render media grid
    echo  '<section>'
        . App\mediaGrid('front-page', $media_query->get_posts())
        . '</section>'
    ;

    // Render pagination
    $pagination = paginate_links([
        'current' => $media_query->get('paged') ? $media_query->get('paged') : 1,
        'total' => $media_query->max_num_pages,
        'mid_size' => 3,
        'type' => 'array',
    ]);
    if ($pagination) {
        echo  '<nav class="pagination">' . implode('', $pagination) . '</nav>';
    }
}
else {
// No media yet.
?>
<section>
    <p class="message"><?= esc_html__('Sad panda! There are no photos or videos uploaded yet.', 'bezirksblaetter'); ?></p>
</section>
<?php
}
