<?php
/**
 * Index page displays:
 * - blog home page (home.php)
 * - search results (search.php)
 *
 * @package Bezirksblätter
 * @version 20170701
 */

get_template_part('partials/nav-breadcrumb');
get_template_part('partials/page-header');

if (!have_posts()) {
?>
    <section>
        <p class="message"><?= esc_html__('Sad panda! No results were found.', 'bezirksblaetter'); ?></p>
        <?php get_search_form(); ?>
    </section>
<?php
}

while (have_posts()) {
    the_post();
    //
    get_template_part('partials/content', is_search() ? 'search' : (get_post_type() === 'post' ? get_post_format() : get_post_type()));
}

// Display posts navigation
the_posts_navigation();
