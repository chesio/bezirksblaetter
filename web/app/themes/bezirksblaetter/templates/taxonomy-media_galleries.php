<?php
/**
 * Template for media galleries custom taxonomy page, ie. page that displays
 * contents of a particular media gallery.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bezirksblätter
 * @version 20200201
 */

/** @var \WP_Term */
$term = $wp_query->get_queried_object();

get_template_part('partials/page-header');

// Section: term description (story behind gallery) and list of subgalleries
?>
<section>
<?php
// Get term description
\App\includeTemplatePart('partials/term-description', ['term' => $term]);

// Fetch and print subgalleries:
$subgalleries = get_terms([
    'taxonomy' => MEDIA_GALLERIES_TAXONOMY,
    'child_of' => $term->term_id, // retrieve all descendants, so padded counts can be properly calculated
]);

if (!empty($subgalleries)) {
?>
    <nav class="subgalleries content"><?= __('Subgalleries', 'bezirksblaetter'); ?>:
        <ul>
        <?php
        wp_list_categories([
            'taxonomy' => MEDIA_GALLERIES_TAXONOMY,
            'parent' => $term->term_id, // retrieve only direct childs of
            'title_li' => '',
            'show_count' => true,
            'orderby' => 'term_group',
            'order' => 'ASC',
            'use_desc_for_title' => true,
            'walker' => new \CeP\Bezirksblaetter\WalkerCategory($subgalleries, MEDIA_GALLERIES_TAXONOMY),
        ]);
        ?>
        </ul>
    </nav>
<?php
}
?>
</section>
<section>
<?php
// Section: gallery media in a grid

// Get posts from main query
$media = $wp_query->posts;

if (count($media)) {
    // Enqueue PhotoSwipe and media grid scripts
    wp_enqueue_script('photoswipe');
    wp_enqueue_script('media-grid');
    // Gallery slug for PhotoSwipe
    $gallery = get_queried_object()->slug;
    // Render media grid
    echo App\mediaGrid($gallery, $media);
}
else {
    // No media in this gallery yet.
?>
    <p class="message"><?= esc_html__('Sad panda! There are no photos or videos in this gallery yet.', 'bezirksblaetter'); ?></p>
<?php
}

// Fetch prev|parent|next gallery
$prev_up_next = [];

// Fetch link to parent gallery (or root gallery page)
if ($term->parent) {
    $prev_up_next[] = App\galleryLink(get_term_by('term_taxonomy_id', $term->parent), 'parent', 'long-arrow-up', 'long-arrow-up');
}
else {
    $gallery_page_id = get_option(B13R_GALLERY_PAGE);
    if ($gallery_page_id) {
        $prev_up_next[] = '<a href="' . get_permalink($gallery_page_id) . '">' . \App\svgUseIcon('long-arrow-up') . '<span class="label">' . get_the_title($gallery_page_id) . '</span>' . \App\svgUseIcon('long-arrow-up') . '</a>';
    }
}

// Get all sibling galleries sorted by `term_group`
$galleries = get_terms([
    'taxonomy'  => MEDIA_GALLERIES_TAXONOMY,
    'parent'    => $term->parent,
    'orderby'   => 'term_group',
    'order'     => 'ASC',
]);

// Get index of current gallery within its siblings
$idx = array_search($term->term_id, array_map(function($term) { return $term->term_id; }, $galleries), true);

if ($idx !== false) { // Should not happen, but better safe than sorry.
    if (isset($galleries[$idx-1])) {
        // Prepend link to previous gallery
        array_unshift($prev_up_next, App\galleryLink($galleries[$idx-1], 'prev', 'long-arrow-left', ''));
    }
    if (isset($galleries[$idx+1])) {
        // Append link to next gallery
        array_push($prev_up_next, App\galleryLink($galleries[$idx+1], 'next', '', 'long-arrow-right'));
    }
}

?>
</section>
<nav class="prev-up-next-gallery"><?= implode(' ', $prev_up_next); ?></nav>