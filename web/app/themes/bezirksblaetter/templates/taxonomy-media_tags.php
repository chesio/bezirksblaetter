<?php
/**
 * Template for media tags custom taxonomy page, ie. page that displays all
 * media that have particular media tag assigned.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Bezirksblätter
 * @version 20200201
 */

/** @var \WP_Term */
$term = $wp_query->get_queried_object();

get_template_part('partials/page-header');

// Section: term description (story behind tag)
?>
<section>
<?php
// Get term description
\App\includeTemplatePart('partials/term-description', ['term' => $term]);
?>
</section>
<section>
<?php
// Section: gallery media in a grid

// Get posts from main query
$media = $wp_query->posts;

if (count($media)) {
    // Enqueue PhotoSwipe and media grid scripts
    wp_enqueue_script('photoswipe');
    wp_enqueue_script('media-grid');
    // Gallery slug for PhotoSwipe
    $gallery = get_queried_object()->slug;
    // Render media grid
    echo App\mediaGrid($gallery, $media);
}
else {
    // No media tagged by this term yet.
?>
    <div class="content">
        <p class="message"><?= sprintf(__('Sad panda! There are no photos or videos tagged with "%s" yet.', 'bezirksblaetter'), $term->name); ?></p>
    </div>
<?php
}
?>
</section>
