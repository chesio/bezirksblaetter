<?php
/**
 * @package Bezirksblätter
 * @version 20170701
 */

while (have_posts()) {
    the_post();

    get_template_part('partials/content-single', get_post_type());
}
