# Bezirksblätter
Git repository with source code of [Bezirksblätter.cz](https://bezirksblaetter.cz) website.

## Requirements
* [PHP](https://www.php.net/) 8.1 or newer
* [WordPress](https://wordpress.org/) 6.2 or newer
