#!/usr/bin/env bash
#
# Verify checksums of WordPress files using WP-CLI and send an alert to site admin in case there's a mismatch.
#
# Usage: Just run this script from project root directory.

# Bail, if admin email is not provided.
ADMIN_EMAIL=$(vendor/bin/wp option get admin_email)
if [ -z "$ADMIN_EMAIL" ]; then
    exit 1
fi

SITE_NAME=$(vendor/bin/wp option get blogname)

# Run the check.
CHECK_RESULT=$(vendor/bin/wp core verify-checksums 2>&1)

# If the check is positive, dispatch an email.
if [ $? -ne 0 ]; then
    echo "$CHECK_RESULT" | mail -s "[$SITE_NAME] Checksums verification alert" $ADMIN_EMAIL
fi

# :)
exit 0
