#!/usr/bin/env bash
#
# Back up the project data: database and uploads directory.
#
# Usage: Just run this script from project root directory.

DATE=$(date +%Y%m%d)
SITE=$(vendor/bin/wp option get home | awk -F/ '{print $3}')

# Back up database.
vendor/bin/wp db export backups/${SITE}_${DATE}_database.sql --add-drop-table

# Back up uploads directory.
tar -cf backups/${SITE}_${DATE}_uploads.tar web/app/uploads

# :)
exit 0
